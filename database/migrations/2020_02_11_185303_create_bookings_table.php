<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('trip_start_date');
            $table->integer('number_of_people');

            $table->unsignedInteger('package_id')->nullable();
            $table->unsignedInteger('adventure_id')->nullable();

            $table->string('first_name', 25);
            $table->string('last_name', 25);
            $table->date('dob');
            $table->string('email');
            $table->string('phone', 30);
            $table->string('country', 30);
            $table->string('passport_number');
            $table->date('passport_expiration_date');
            $table->text('mailing_address');
            $table->string('emergency_contact_name', 50);
            $table->string('emergency_contact_relation', 25);
            $table->string('emergency_contact_phone', 30);

            $table->date('arrival_date');
            $table->time('arrival_time');
            $table->string('arrival_flight_number');
            $table->string('pickup', 10)->default(false);

            $table->date('departure_date');
            $table->time('departure_time');
            $table->string('departure_flight_number');
            $table->string('drop_off', 10)->default(false);

            $table->string('insurance', 30);
            $table->mediumText('message')->nullable();
            $table->string('reference', 20);
            $table->boolean('status')->default(false);

            $table->timestamps();

            $table->foreign('package_id')
                ->references('id')
                ->on('packages');

            $table->foreign('adventure_id')
                ->references('id')
                ->on('activities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
