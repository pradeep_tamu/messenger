<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->Increments('id');
            $table->unsignedInteger('destination_id');
            $table->string('name');
            $table->integer('cost');
            $table->string('duration');
            $table->string('capacity');
            $table->text('description');
            $table->text('overview');
            $table->text('itineraries');
            $table->text('cost_details');
            $table->text('practical_info');
            $table->string('filename')->nullable();
            $table->string('slug')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('keyword')->nullable();
            $table->timestamps();

            $table->foreign('destination_id')
                ->references('id')
                ->on('destinations')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
