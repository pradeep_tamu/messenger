<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Permission;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo PHP_EOL, 'seeding users...';

        $user = User::create(
            [
                'name' => 'Pradeep Gurung',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('password'),
                'type' => 'admin',
                'active' => 1
            ]
        );

        $user = User::create(
            [
                'name' => 'Bivek Shrestha',
                'email' => 'user@gmail.com',
                'password' => bcrypt('password'),
                'active' => 1
            ]
        );

        $user = User::create(
            [
                'name' => 'User 1',
                'email' => 'user1@gmail.com',
                'password' => bcrypt('password'),
                'active' => 1
            ]
        );

        $user = User::create(
            [
                'name' => 'User 2',
                'email' => 'user2@gmail.com',
                'password' => bcrypt('password'),
                'active' => 0
            ]
        );
        echo PHP_EOL, 'Users seeded';
    }
}
