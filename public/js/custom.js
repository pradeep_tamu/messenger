(function ($) {
    'use strict';
    // //loader
    $(document).ready(function() {
        window.onload = function () {
            $('#preloader').fadeOut(500, function(){ $('#preloader').remove(); } );
        }
    });

    var nav_offset_top = $(".web-navigation").height()+40;
    //* Navbar Fixed
    function navbarFixed(){
        if ( $(".navbar").length ){
            $(window).scroll(function() {
                var scroll = $(window).scrollTop();
                if (scroll >= nav_offset_top ) {
                    $(".navbar").addClass("navbar_fixed");
                } else {
                    $(".navbar").removeClass("navbar_fixed");
                }
            });
        }
    }
    navbarFixed();

    if ($.fn.owlCarousel) {
        var landingSlider = $('.landing-slides');
        landingSlider.owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            smartSpeed: 1000,
            nav: true
        });

        landingSlider.on('translate.owl.carousel', function () {
            var layer = $("[data-animation]");
            layer.each(function () {
                var anim_name = $(this).data('animation');
                $(this).removeClass('animated ' + anim_name).css('opacity', '0');
            });
        });

        $("[data-delay]").each(function () {
            var anim_del = $(this).data('delay');
            $(this).css('animation-delay', anim_del);
        });

        $("[data-duration]").each(function () {
            var anim_dur = $(this).data('duration');
            $(this).css('animation-duration', anim_dur);
        });

        landingSlider.on('translated.owl.carousel', function () {
            var layer = landingSlider.find('.owl-item.active').find("[data-animation]");
            layer.each(function () {
                var anim_name = $(this).data('animation');
                $(this).addClass('animated ' + anim_name).css('opacity', '1');
            });
        });

        var landingSlide = function landingSlide() {
            $('.owl-item').removeClass('prev next');
            var currentSlide = $('.landing-slides .owl-item.active');
            currentSlide.next('.owl-item').addClass('next');
            currentSlide.prev('.owl-item').addClass('prev');
            var nextSlideImg = $('.owl-item.next').find('.single-landing-slide').attr('data-img-url');
            var prevSlideImg = $('.owl-item.prev').find('.single-landing-slide').attr('data-img-url');
            $('.owl-nav .owl-prev').css({
                background: 'url(' + prevSlideImg + ')'
            });
            $('.owl-nav .owl-next').css({
                background: 'url(' + nextSlideImg + ')'
            });
        }

        landingSlide();

        landingSlider.on('translated.owl.carousel', function () {
            landingSlide();
        });
    }


    $(".single-project-slide").on("mouseenter", function () {
        $(".single-project-slide").removeClass("active");
        $(this).addClass("active");
    });


    $('a[href="#"]').on('click', function ($) {
        $.preventDefault();
    });

    //package slider
    function packageSlider() {
        if ($('.package_display').length) {

                $('.package_display').owlCarousel({
                    loop :true,
                    margin: 30,
                    items : 3,
                    nav :false,
                    autoplay: true,
                    smartSpeed: 500,
                    dots: false,
                    responsiveClass: true,
                    responsive:{
                        0:{
                            items: 1,
                        },400:{
                            items: 1,
                        },575:{
                            items: 1,
                        },768:{
                            items: 2,
                        },1100:{
                            items: 2,
                        },1200:{
                            items: 3,
                        }
                    }

                });
        }
    }
    packageSlider();

    // search packages
    $(document).ready(function(){

        $('#search-package').keyup(function(){
            let query = $(this).val();
            if(query != '')
            {
                let _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"/fetch-search-package",
                    method:"POST",
                    data:{query:query, _token:_token},
                    success:function(data){
                        $('#all-package-list').hide();
                        $('.pagination').hide();
                        $('#search-result').fadeIn();
                        let output = jQuery.parseJSON(data);
                        let result = '';
                        $.each(output, function (index, element) {
                            let desc = element.description;
                            result += '<div class="single-package-area d-flex align-items-center mb-50">'+
                               '<!-- pacakge Thumbnail -->'+
                               '<div class="package-thumbnail">'+
                                   '<img src="/img/package/'+element.filename+'" alt="">'+
                               '</div>'+
                               '<!-- package Content -->'+
                               '<div class="package-content">'+
                                   '<h2>'+element.name+'</h2>'+
                                   '<span>From <h4>'+element.cost+'</h4></span>'+
                                   '<div class="package-feature">'+
                                       '<h6>Duration: <span>'+element.duration+'</span></h6>'+
                                       '<h6>Capacity: <span>'+element.capacity+'</span></h6>'+
                                       '<h6 class="desc">Description: <span class="desc" style="width:100%">'+ desc.substring(0,200) +'</span></h6>'+
                                   '</div>'+
                                   '<button><a href="'+'/package/'+element.id +'" class="btn view-detail-btn">View Details <i class="fas fa-long-arrow-alt-right"></i></a>'+
                                   '</button>'+
                               '</div>'+
                           '</div>';
                        });
                        $('#search-result').html(result);

                    }
                });
            }
            else {
                $('#all-package-list:hidden').show();
                $('.pagination').show();
                $('#search-result').hide();
            }
        });

    });

    // search destination
    $(document).ready(function(){

        $('#search-destination').keyup(function(){
            let query = $(this).val();
            if(query)
            {
                let _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"/fetch-search-destination",
                    method:"POST",
                    data:{query:query, _token:_token},
                    success:function(data){
                        $('#all-destination-list').hide();
                        $('.pagination').hide();
                        $('#search-result').fadeIn();
                        let output = jQuery.parseJSON(data);
                        let result = '';
                        $.each(output, function (index, element) {
                            let desc = element.description;
                            result += '<div class="single-project-slide active bg-img col-sm-4">'+
                               '<div class="project-content">'+
                                   '<img src="/img/destination/'+ element.filename+'"'+
                                        'alt="">'+
                               '</div>'+
                               '<!-- Hover Effects -->'+
                               '<div class="hover-effects">'+
                                   '<div class="text">'+
                                       '<h3>'+element.name+'</h3>'+
                                       '<p>'+ desc.substring(0,400) +'</p>'+
                                   '</div>'+
                                   '<a href="/destination/'+element.id+'" class="btn project-btn">Discover'+
                                       'Now <i class="fa fa-long-arrow-right"'+
                                              'aria-hidden="true"></i></a>'+
                               '</div>'+
                           '</div>';
                        });
                        $('#search-result').html(result);
                    }
                });
            }
            else {
                $('#all-destination-list:hidden').show();
                $('.pagination').show();
                $('#search-result').hide();
            }
        });

    });

    // search adventures
    $(document).ready(function(){

        $('#search-adventure').keyup(function(){
            let query = $(this).val();
            if(query)
            {
                let _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"/fetch-search-adventure",
                    method:"POST",
                    data:{query:query, _token:_token},
                    success:function(data){
                        $('#all-adventures-list').hide();
                        $('.pagination').hide();
                        $('#search-result').fadeIn();
                        let output = jQuery.parseJSON(data);
                        let result = '';
                        $.each(output, function (index, element) {
                            let desc = element.description;
                            result += '<div class="col-12 col-lg-9">'+
                               '<!-- Single Room Area -->'+
                               '<div class="single-adventure-area d-flex align-items-center mb-50">'+
                                   '<!-- Room Thumbnail -->'+
                                   '<div class="adventure-thumbnail">'+
                                       '<img src="/img/adventure/'+ element.filename+'" alt="">'+
                                   '</div>'+
                                   '<!-- Room Content -->'+
                                   '<div class="adventure-content">'+
                                       '<h2>'+element.name+'</h2>'+
                                       '<h5 class="desc">Description: <span class="desc" style="width:100%">'+ desc.substring(0,300) +'</span>'+
                                       '</h5>'+
                                   '</div>'+
                               '</div>'+
                           '</div>'+
                           '<div class="details col-12 col-lg-3">'+
                               '<h2>From</h2>'+
                               '<h4>$'+element.cost+'</h4>'+
                               '<a href="/adventure/'+ element.id+'"><button>View Details <i class="fas fa-long-arrow-alt-right"></i></button></a>'+
                           '</div>';
                        });
                        $('#search-result').html(result);
                    }
                });
            }
            else {
                $('#all-adventures-list').show();
                $('.pagination').show();
                $('#search-result').hide();
            }
        });

    });

    // landing page search option
    $(document).ready(function(){

        setTimeout(function(){
            $('.alert').hide('slow');
        },5000);

        $('#destination').change(function () {
            let query = $(this).val();
            if(query != 0)
            {
                let _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"/show-available-package",
                    method:"POST",
                    data:{query:query, _token:_token},
                    success:function(data){
                        $('#package').prop('disabled', false);
                        let output = jQuery.parseJSON(data);
                        let result = '<option value="0">Check Available Packages</option>';
                        $.each(output, function (index, element) {
                            result += '<option value="' + element.id + '">' + element.name + '</option>';
                        });
                        $('#package').html(result);

                    }
                });
            }
            else {
                $('#package').prop('disabled', true);
                $('#package').html();
                $('#duration').prop('disabled', true);
                $('#duration').val('');
            }
        });
    });

    $(document).ready(function(){

        $('#package').change(function () {
            let query = $(this).val();
            if(query != 0)
            {
                let _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"/show-package-duration",
                    method:"POST",
                    data:{query:query, _token:_token},
                    success:function(data){
                        $('#duration').prop('disabled', false);
                        let result = jQuery.parseJSON(data);
                        let output = ' <a href="/package/' + result.id + '" class="form-control btn landing-btn w-100"> Find Your Tour </a>';
                        $('#duration').val(result.duration);
                        $('#findbutton').html(output);
                    }
                });
            }
            else {

                $('#duration').prop('disabled', true);
                $('#duration').val('');
                let output = '<button class="form-control btn landing-btn w-100" disabled>Find Your Tour</button>';
                $('#findbutton').html(output);

            }
        });
    });



})(jQuery);
