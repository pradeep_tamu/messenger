@extends('backend.layout.master')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Blog List</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ Route('blog.list') }}">Blog</a></li>
                        <li class="breadcrumb-item active">Blog</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <!-- /.box-header -->
                    <div class="box-body margin-top-20">
                        <div class="table-responsive">
                            <table id="listDataTable"
                                   class="table table-bordered table-striped list_view_table display responsive no-wrap"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="5%">Image</th>
                                    <th width="10%">Title</th>
                                    <th width="15%">Written By</th>
                                    <th width="10%">Created at</th>
                                    <th width="10%">Updated at</th>
                                    <th class="notexport" width="5%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($blogs as $blog)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>
                                            <img class="img-responsive center" style="height: 35px; width: 35px;"
                                                 src="{{ asset('img/blog/'. $blog->image)}}" alt="">
                                        </td>
                                        <td>{{ $blog->title }}</td>
                                        <td>{{ $blog->writer }}</td>
                                        <td>{{ \Carbon\Carbon::parse($blog->created_at)->diffForHumans() }}</td>
                                        <td>{{ \Carbon\Carbon::parse($blog->updated_at)->diffForHumans() }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <form method="POST" action="{{ route('blog.destroy', $blog->id) }}">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete">
                                                        <i class="fa fa-fw fa-trash"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
