@extends('backend.layout.master')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Adventure Booking</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Adventure Bookings</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-tools float-lg-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body margin-top-20">
                        <div class="table-responsive">
                            <table id="myTable"
                                   class="table table-bordered table-striped list_view_table display responsive no-wrap"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="15%">Adventure Name</th>
                                    <th width="10%">Name</th>
                                    <th width="10%">Email</th>
                                    <th width="10%">Country</th>
                                    <th width="10%">Phone</th>
                                    <th width="10%">Status</th>
                                    <th class="notexport" width="10%">View</th>
                                    <th class="notexport" width="10%">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bookings as $booking)
                                    <tr>
                                        <td>
                                            {{$loop->iteration}}
                                        </td>
                                        <td>{{\App\Activities::find($booking->adventure_id)->name}}</td>
                                        <td>{{$booking->first_name}} {{$booking->last_name}}</td>
                                        <td>{{$booking->email}}</td>
                                        <td>{{$booking->country}}</td>
                                        <td>{{$booking->phone}}</td>
                                        <td>
                                            @if($booking->status)
                                                <span class="px-2 py-1 bg-success rounded-pill">Completed</span>
                                            @else
                                                <span class="px-2 py-1 bg-danger rounded-pill">Pending</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a title="View Detail"
                                               href="{{ Route('booking.detail',$booking->id) }}"
                                               class="btn btn-info btn-sm"><i
                                                    class="fa fa-eye"></i></a>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <form class="myAction" method="POST"
                                                      action="{{Route('adventure_booking.destroy', $booking->id)}}">
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete">
                                                        <i class="fa fa-fw fa-trash"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
