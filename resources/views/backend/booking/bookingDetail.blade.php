@extends('backend.layout.master')

@section('content')
    <div class="pb-5">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Booking</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Bookings</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content p-2" style="overflow-y: scroll; overflow-x: hidden">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header mb-2">
                            <div class="box-tools float-lg-right mb-2">
                                @if(!$booking->status)
                                    <a class="btn btn-primary" href="{{route('booking.status', $booking->id)}}">Compete</a>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body margin-top-20">
                            <div class="table-responsive table">
                                <table id="listTable"
                                       class="table table-bordered table-striped list_view_table display responsive no-wrap"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th width="40%">Particulars</th>
                                        <th>Detail</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($booking->package_id)
                                        <tr>
                                            <td>Package Name</td>
                                            <td>{{\App\Package::find($booking->package_id)->name}}</td>
                                        </tr>
                                    @endif
                                    @if($booking->adventure_id)
                                        <tr>
                                            <td>Adventure Name</td>
                                            <td>{{\App\Adventure::find($booking->adventure_id)->name}}</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td>
                                            Trip Start Date
                                        </td>
                                        <td>{{$booking->trip_start_date}}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Number of People
                                        </td>
                                        <td>{{$booking->number_of_people}}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Name
                                        </td>
                                        <td>{{$booking->first_name}} {{$booking->last_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Date of Birth</td>
                                        <td>{{$booking->dob}}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Email
                                        </td>
                                        <td>{{$booking->email}}</td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td>{{$booking->phone}}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Country
                                        </td>
                                        <td>{{$booking->country}}</td>
                                    </tr>
                                    <tr>
                                        <td>Passport Number</td>
                                        <td>{{$booking->passport_number}}</td>
                                    </tr>
                                    <tr>
                                        <td>Passport Expiration Date</td>
                                        <td>{{$booking->passport_expiration_date}}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mailing Address
                                        </td>
                                        <td>{{$booking->mailing_address}}</td>
                                    </tr>
                                    <tr>
                                        <td>Emergency Contact Name</td>
                                        <td>{{$booking->emergency_contact_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Emergency Contact Relation</td>
                                        <td>{{$booking->emergency_contact_relation}}</td>
                                    </tr>
                                    <tr>
                                        <td>Emergency Contact number</td>
                                        <td>{{$booking->emergency_contact_phone}}</td>
                                    </tr>

                                    <tr>
                                        <td>Arrival Date and Time</td>
                                        <td>{{ $booking->arrival_date }} {{ $booking->arrival_time }}</td>
                                    </tr>
                                    <tr>
                                        <td>Arrival Flight Number</td>
                                        <td>{{ $booking->arrival_flight_number }}</td>
                                    </tr>
                                    <tr>
                                        <td>Pickup</td>
                                        <td>{{$booking->pickup}}</td>
                                    </tr>

                                    <tr>
                                        <td>Departure Date and Time</td>
                                        <td>{{ $booking->departure_date }} {{ $booking->departure_time }}</td>
                                    </tr>
                                    <tr>
                                        <td>Departure Flight Number</td>
                                        <td>{{ $booking->departure_flight_number }}</td>
                                    </tr>
                                    <tr>
                                        <td>Pickup</td>
                                        <td>{{$booking->drop_off}}</td>
                                    </tr>

                                    <tr>
                                        <td>Insurance</td>
                                        <td>{{$booking->insurance}}</td>
                                    </tr>
                                    <tr>
                                        <td>Message</td>
                                        <td>{{$booking->messgae}}</td>
                                    </tr>
                                    <tr>
                                        <td>Reference</td>
                                        <td>{{$booking->reference}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
@endsection
