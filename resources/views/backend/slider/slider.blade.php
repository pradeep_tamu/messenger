
@extends('backend.layout.master')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Slider's Image List</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Image Slider</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-tools float-lg-left">
                            @if(Session::has('message'))
                                {{ Session::get('message') }}
                            @endif
                        </div>
                        <div class="box-tools float-lg-right">
                            <a class="btn btn-info btn-sm" href="{{ Route('slider.create') }}"><i
                                        class="fa fa-plus-circle"></i> Add New Image</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body margin-top-20">
                        <div class="table-responsive">
                            <table id="listDataTable"
                                   class="table table-bordered table-striped list_view_table display responsive no-wrap"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th width="50%">Image</th>
                                    <th class="notexport" width="20%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sliders as $slider)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>
                                            <img class="img-responsive center" style="height: 35px; width: 35px;" src="{{ asset('/img/slider/'. $slider->filename) }}" alt="slider image">
                                        </td>

                                        <td>
                                            <div class="btn-group">
                                                <form method="POST" action="{{route('slider.destroy', $slider->id)}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete">
                                                        <i class="fa fa-fw fa-trash"></i>
                                                    </button>
                                                </form>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection