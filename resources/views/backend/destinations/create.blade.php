@extends('backend.layout.master')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Destination @if($destination) Update @else Add New @endif</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ Route('destinations.list') }}">Destinations</a></li>
                        <li class="breadcrumb-item active"> @if($destination) Update @else Add New @endif</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="container">
        <div class="card">
            <div class="card-header">{{ __('Add New Destination') }}</div>

            <div class="card-body">
                <form action=" @if($destination){{ route('destinations.edit', $destination->id) }} @else {{route('destinations.create')}} @endif"
                      method="post" enctype="multipart/form-data">
                    @csrf
                    @if($destination)
                        @method('PATCH')
                    @endif
                    <h5 class="font-weight-bold">Seo Meta Section</h5>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group has-feedback">
                                <label for="title">Meta Title</label>
                                <input autofocus type="text" class="form-control" name="meta_title"
                                       placeholder="meta title"
                                       value="@if($destination){{ $destination->meta_title }}@else{{old('title')}}@endif"
                                       minlength="2" maxlength="255">
                                <span class="text-danger">{{ $errors->first('meta_title') }}</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group has-feedback">
                                <label for="title">Meta Description</label>
                                <input autofocus type="text" class="form-control" name="meta_description"
                                       placeholder="meta meta_description"
                                       value="@if($destination){{ $destination->meta_description }}@else{{old('meta_description')}}@endif"
                                       minlength="2" maxlength="255">
                                <span class="text-danger">{{ $errors->first('meta_description') }}</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group has-feedback">
                                <label for="title">Meta Keywords</label>
                                <input autofocus type="text" class="form-control" name="keyword"
                                       placeholder="meta keyword"
                                       value="@if($destination){{ $destination->keyword }}@else{{old('keyword')}}@endif"
                                       minlength="2" maxlength="255">
                                <span class="text-danger">{{ $errors->first('keyword') }}</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">

                        <div class="col-md-6">
                            <label for="name"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <input id="name" type="text"
                                   class="form-control @error('name') is-invalid @enderror" name="name"
                                   value="@if($destination){{ $destination->name }}@else{{old('name')}}@endif"
                                   required autofocus>

                            <span class="invalid-feedback d-block"
                                  role="alert">{{ $errors->first('name') }}</span>

                        </div>
                        <div class="col-md-6">
                            <label for="descriptions"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                            <textarea id="descriptions" type="text" cols="30" rows="10"
                                      class="form-control @error('name') is-invalid @enderror"
                                      name="description" required autofocus>
                                        @if($destination){{ $destination->description }}@else{{old('description')}}@endif
                                    </textarea>
                            <span class="invalid-feedback d-block"
                                  role="alert">{{ $errors->first('description') }}</span>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Destination Image') }}</label>
                            <input id="image" type="file"
                                   class="form-control border-0 @error('image') is-invalid @enderror"
                                   name="image"
                                   value="@if($destination){{ $destination->filename }}@else{{old('filename')}}@endif">

                            <span class="invalid-feedback d-block"
                                  role="alert">{{ $errors->first('image') }}</span>

                        </div>
                    </div>

                    <div class="row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa @if($destination) fas-refresh @else fas-plus-circle @endif"></i> @if($destination)
                                    Update @else Add @endif
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection




