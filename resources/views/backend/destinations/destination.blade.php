@extends('backend.layout.master')

@section('content')


    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Destination Lists</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Destination</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-tools float-lg-right">
                            <a class="btn btn-info btn-sm" href="{{ Route('destinations.create') }}"><i
                                    class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body margin-top-20">
                        <div class="table-responsive">
                            <table id="listDataTable"
                                   class="table table-bordered table-striped list_view_table display responsive no-wrap"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th width="5%">Number</th>
                                    <th width="15%">Name</th>
                                    <th width="25%">Description</th>
                                    <th width="10%">Photos</th>
                                    <th align="center" class="notexport" width="20%">Action: Edit | Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($destinations as $destination)
                                    <tr>
                                        <td>
                                            {{$loop->iteration}}
                                        </td>
                                        <td>{{ $destination->name }}</td>
                                        <td>{!! $destination->description !!}</td>
                                        <td>
                                            <img class="img-responsive center" style="height: 35px; width: 35px;"
                                                 src="{{ asset('/img/destination/'. $destination->filename) }}"
                                                 alt="destination image">
                                        </td>
                                        <td align="center">
                                            <div class="btn-group">
                                                <a title="Edit" href="{{ Route('destinations.edit',$destination->id) }}"
                                                   class="btn btn-info btn-sm"><i
                                                        class="fa fa-edit"></i></a>
                                            </div>
                                            |
                                            <div class="btn-group">
                                                <form class="myAction" method="POST"
                                                      action="{{Route('destinations.destroy', $destination->id)}}">
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete">
                                                        <i class="fa fa-fw fa-trash"></i>
                                                    </button>
                                                </form>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
