@extends('backend.layout.master')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Package @if($package) Update @else Add New @endif</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ Route('package.list') }}">Packages</a></li>
                        <li class="breadcrumb-item active"> @if($package) Update @else Add New @endif</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form action=" @if($package){{ Route('package.edit', $package->id) }} @else {{Route('package.create')}} @endif"
                              method="post" enctype="multipart/form-data">
                            @csrf
                            @if($package)
                                @method('PATCH')
                            @endif
                            <h5 class="font-weight-bold">Seo Meta Section</h5>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="title">Meta Title</label>
                                        <input autofocus type="text" class="form-control" name="meta_title" placeholder="meta title" value="@if($package){{ $package->meta_title }}@else{{old('title')}}@endif" minlength="2" maxlength="255">
                                        <span class="text-danger">{{ $errors->first('meta_title') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="title">Meta Description</label>
                                        <input autofocus type="text" class="form-control" name="meta_description" placeholder="meta meta_description" value="@if($package){{ $package->meta_description }}@else{{old('meta_description')}}@endif" minlength="2" maxlength="255">
                                        <span class="text-danger">{{ $errors->first('meta_description') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="title">Meta Keywords</label>
                                        <input autofocus type="text" class="form-control" name="keyword" placeholder="meta keyword" value="@if($package){{ $package->keyword }}@else{{old('keyword')}}@endif" minlength="2" maxlength="255">
                                        <span class="text-danger">{{ $errors->first('keyword') }}</span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="name">Name</label>
                                        <input autofocus type="text" class="form-control" name="name" placeholder="name" value="@if($package){{ $package->name }}@else{{old('name')}}@endif" required minlength="2" maxlength="255">
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="destination_id">Destinations
                                        </label>
                                        {!! Form::select('destination_id', $destinations, $destination , ['placeholder' => 'Pick a destination...','class' => 'form-control select2', 'required' => 'true']) !!}
                                        <span class="text-danger">{{ $errors->first('destination_id') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="image">Photo Upload
                                        </label>
                                        <input id="image" type="file"
                                               class="form-control border-0 @error('image') is-invalid @enderror" name="image"
                                               value="@if($package){{ $package->filename }}@else{{old('image')}}@endif" @if(!$package) required @endif
                                               multiple>
                                        <span class="text-danger">{{ $errors->first('image') }}</span>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="cost">Price</label>
                                        <input autofocus type="text" class="form-control" name="cost" placeholder="cost" value="@if($package){{ $package->cost }}@else{{old('cost')}}@endif" required>
                                        <span class="text-danger">{{ $errors->first('cost') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="duration">Duration</label>
                                        <input autofocus type="text" class="form-control" name="duration" placeholder="duration" value="@if($package){{ $package->duration }}@else{{old('duration')}}@endif" required>
                                        <span class="text-danger">{{ $errors->first('duration') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="capacity">capacity</label>
                                        <input autofocus type="text" class="form-control" name="capacity" placeholder="capacity" value="@if($package){{ $package->capacity }}@else{{old('capacity')}}@endif" required>
                                        <span class="text-danger">{{ $errors->first('capacity') }}</span>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="description">Description</label>
                                        <textarea autofocus type="text" rows="8" class="form-control" name="description" id="description" required>
                                            @if($package){{ $package->description }}@else{{old('description')}}@endif
                                        </textarea>
                                        <span class="text-danger">{{ $errors->first('description') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="overview">Overview</label>
                                        <textarea autofocus type="text" rows="8" class="form-control" name="overview" id="overview"  required >
                                            @if($package){{ $package->overview }}@else{{old('overview')}}@endif
                                        </textarea>
                                        <span class="text-danger">{{ $errors->first('overview') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="cost_details">Cost Details</label>
                                        <textarea autofocus type="text" rows="8" class="form-control" name="cost_details" id="cost_details" required>
                                            @if($package){{ $package->cost_details }}@else{{old('cost_details')}}@endif
                                        </textarea>
                                        <span class="text-danger">{{ $errors->first('cost_details') }}</span>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="itineraries">Itineraries</label>
                                        <textarea autofocus type="text" rows="8" class="form-control" name="itineraries" id="itineraries" required>
                                            @if($package){{ $package->itineraries }}@else{{old('itineraries')}}@endif
                                        </textarea>
                                        <span class="text-danger">{{ $errors->first('itineraries') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="practical_info">Practical Information</label>
                                        <textarea autofocus type="text" rows="8" class="form-control" name="practical_info" id="practical_info"  required >
                                            @if($package){{ $package->practical_info }}@else{{old('practical_info')}}@endif
                                        </textarea>
                                        <span class="text-danger">{{ $errors->first('practical_info') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 offset-md-12">
                                    <button type="submit" class="btn btn-primary float-sm-right">
                                        <i class="fa @if($package) fas-refresh @else fas-plus-circle @endif"></i> @if($package)
                                            Update @else Add Package @endif
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        CKEDITOR.replace( 'overview' );
        CKEDITOR.replace( 'cost_details' );
        CKEDITOR.replace( 'itineraries' );
        CKEDITOR.replace( 'practical_info' );
    </script>
@endsection
