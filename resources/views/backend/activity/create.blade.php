@extends('backend.layout.master')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Activity @if($activity) Update @else Add New @endif</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ Route('activity.list') }}">Activities</a></li>
                        <li class="breadcrumb-item active"> @if($activity) Update @else Add New @endif</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="container">
        <div class="card">
            <div class="card-header">{{ __('Add New Activity') }}</div>

            <div class="card-body">
                <form action=" @if($activity){{ Route('activity.edit', $activity->id) }} @else {{Route('activity.create')}} @endif"
                      method="post" enctype="multipart/form-data">
                    @csrf
                    @if($activity)
                        @method('PATCH')
                    @endif

                    <h5 class="font-weight-bold">Seo Meta Section</h5>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group has-feedback">
                                <label for="title">Meta Title</label>
                                <input autofocus type="text" class="form-control" name="meta_title"
                                       placeholder="meta title"
                                       value="@if($activity){{ $activity->meta_title }}@else{{old('title')}}@endif"
                                       minlength="2" maxlength="255">
                                <span class="text-danger">{{ $errors->first('meta_title') }}</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group has-feedback">
                                <label for="title">Meta Description</label>
                                <input autofocus type="text" class="form-control" name="meta_description"
                                       placeholder="meta meta_description"
                                       value="@if($activity){{ $activity->meta_description }}@else{{old('meta_description')}}@endif"
                                       minlength="2" maxlength="255">
                                <span class="text-danger">{{ $errors->first('meta_description') }}</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group has-feedback">
                                <label for="title">Meta Keywords</label>
                                <input autofocus type="text" class="form-control" name="keyword"
                                       placeholder="meta keyword"
                                       value="@if($activity){{ $activity->keyword }}@else{{old('keyword')}}@endif"
                                       minlength="2" maxlength="255">
                                <span class="text-danger">{{ $errors->first('keyword') }}</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="name" class="col-form-label text-md-right">{{ __('Name') }}</label>

                            <input id="name" type="text"
                                   class="form-control @error('name') is-invalid @enderror" name="name"
                                   value="@if($activity){{ $activity->name }}@else{{old('name')}}@endif"
                                   required
                                   autocomplete="name" autofocus>

                            <span class="invalid-feedback d-block" role="alert">{{ $errors->first('name') }}</span>
                        </div>
                        <div class="col-md-6">
                            <label for="description"
                                   class="col-form-label text-md-right">{{ __('Description') }}</label>
                            <textarea id="description" type="text" rows="5"
                                      class="form-control @error('name') is-invalid @enderror"
                                      name="description"
                                      required
                                      autofocus>@if($activity){{$activity->description}}@else{{old('description')}}@endif
                                    </textarea>

                            <span class="invalid-feedback d-block"
                                  role="alert">{{ $errors->first('description') }}</span>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="name" class="col-form-label text-md-right">{{ __('Location') }}</label>

                            <input id="location" type="text"
                                   class="form-control @error('location') is-invalid @enderror" name="location"
                                   value="@if($activity){{ $activity->location }}@else{{old('location')}}@endif"
                                   required
                                   autocomplete="location" autofocus>

                            <span class="invalid-feedback d-block" role="alert">{{ $errors->first('location') }}</span>

                        </div>
                        <div class="col-md-4">
                            <label for="cost" class="col-form-label text-md-right">{{ __('Cost') }}</label>

                            <input id="cost" type="text"
                                   class="form-control @error('cost') is-invalid @enderror" name="cost"
                                   value="@if($activity){{ $activity->cost }}@else{{old('cost')}}@endif"
                                   required
                                   autocomplete="cost" autofocus>

                            <span class="invalid-feedback d-block" role="alert">{{ $errors->first('cost') }}</span>

                        </div>
                        <div class="col-md-4">
                            <label for="videos" class="col-form-label text-md-right">{{ __('Videos') }}</label>

                            <input id="videos" type="text"
                                   class="form-control @error('videos') is-invalid @enderror" name="videos"
                                   value="@if($activity){{ $activity->videos }}@else{{old('videos')}}@endif"
                                   required
                                   autocomplete="videos" autofocus>

                            <span class="invalid-feedback d-block" role="alert">{{ $errors->first('videos') }}</span>

                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4">
                            <label for="season" class="col-form-label text-md-right">{{ __('Season') }}</label>

                            <input id="season" type="text"
                                   class="form-control @error('season') is-invalid @enderror" name="season"
                                   value="@if($activity){{ $activity->season }}@else{{old('season')}}@endif"
                                   required
                                   autocomplete="season" autofocus>

                            <span class="invalid-feedback d-block" role="alert">{{ $errors->first('season') }}</span>

                        </div>
                        <div class="col-md-4">
                            <label for="transportation"
                                   class="col-form-label text-md-right">{{ __('Transportation') }}</label>
                            <input id="transportation" type="text"
                                   class="form-control @error('transportation') is-invalid @enderror"
                                   name="transportation"
                                   value="@if($activity){{ $activity->transportation }}@else{{old('transportation')}}@endif"
                                   required
                                   autocomplete="transportation" autofocus>

                            <span class="invalid-feedback d-block"
                                  role="alert">{{ $errors->first('transportation') }}</span>

                        </div>
                        <div class="col-md-4">
                            <label for="start_end"
                                   class="col-form-label text-md-right">{{ __('Start/End') }}</label>
                            <input id="start_end" type="text"
                                   class="form-control @error('start_end') is-invalid @enderror" name="start_end"
                                   value="@if($activity){{ $activity->start_end }}@else{{old('start_end')}}@endif"
                                   required>

                            <span class="invalid-feedback d-block" role="alert">{{ $errors->first('start_end') }}</span>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="image"
                                   class="col-form-label text-md-right">{{ __('Activity Image') }}</label>
                            <input id="image" type="file"
                                   class="form-control border-0 @error('image') is-invalid @enderror" name="image"
                                   value="@if($activity){{ $activity->filename }}@else{{old('image')}}@endif">

                            <span class="invalid-feedback d-block" role="alert">{{ $errors->first('image') }}</span>

                        </div>
                    </div>

                    <div class="row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa @if($activity) fas-refresh @else fas-plus-circle @endif"></i> @if($activity)
                                    Update @else Add @endif
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
