
{{$msg}}
The Booking details are:

@if(isset($package))
<p>
<Strong> Package Name: </Strong> {{ $package->name }}
</p>
@endif

@if(isset($adventure))
<p>
<Strong> Package Name: </Strong> {{ $adventure->name }}
</p>
@endif

<p>
<Strong>Trip Start Date:</Strong> {{ $booking->trip_start_date}}
</p>
<p>
<Strong>Number of People:</Strong> {{ $booking->number_of_people}}
</p>

<p>
<Strong>Name:</Strong> {{ $booking->first_name}} {{$booking->last_name }}
</p>
<p>
<Strong> Email: </Strong> {{ $booking->email }}
</p>
<p>
<Strong> Phone: </Strong> {{ $booking->phone }}
</p>
<p>
<Strong> Country: </Strong> {{ $booking->country }}
</p>
<p>
<Strong> Passport Number: </Strong> {{ $booking->passport_number }}
</p>
<p>
<Strong> Passport Expiration Date: </Strong> {{ $booking->passport_expiration_date }}
</p>
<p>
<Strong> Mailing Address: </Strong> {{ $booking->mailing_address }}
</p>
<p>
<Strong> Emergency Contact Name: </Strong> {{ $booking->emergency_contact_name }}
</p>
<p>
<Strong> Emergency Contact Relation: </Strong> {{ $booking->emergency_contact_relation }}
</p>
<p>
<Strong> Emergency Contact Number: </Strong> {{ $booking->emergency_contact_phone }}
</p>

<p>
<Strong> Arrival Date and
Time: </Strong> {{ $booking->arrival_date }} {{ $booking->arrival_time }}
</p>
<p>
<Strong> Arrival Flight Number: </Strong> {{ $booking->arrival_flight_number }}
</p>
<p>
<Strong> Pickup: </Strong> {{ $booking->pickup }}
</p>


<p>
<Strong> Departure Date and
Time: </Strong> {{ $booking->departure_date }} {{ $booking->departure_time }}
</p>
<p>
<Strong> Departure Flight Number: </Strong> {{ $booking->departure_flight_number }}
</p>
<p>
<Strong> Drop off: </Strong> {{ $booking->pickup }}
</p>

<p>
<Strong> Insurance: </Strong> {{ $booking->insurance }}
</p>

<p>
Thank you for booking with us,

With Regards,
Messanger Tours and Travel
</p>







