@extends('frontend.layout.layout')

@section('title', 'Tours | Travel | Visit Nepal | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

    <!-- About Us Area Start -->
    <section class="privacy mt-5 mb-5">
        <div class="privacy-wrapper container">
            <h3><strong>
                    PRIVACY POLICY
                </strong>
            </h3>
            <p>Thank you for choosing to be part of our community at Messanger Tours and Travel. We are committed to
                protecting your personal information and your right to privacy. If you have any questions or concerns
                about
                our policy, or our practices with regards to your personal information, please contact us at
                msntraveltours@gmail.com.</p>
            <p>
                When you visit our website <a href="http://msntraveltours.com/">www.msntraveltours.com</a> , and use our
                services, you trust us with your personal
                information. We take your privacy very seriously. In this privacy policy, we seek to explain to you in
                the
                clearest way possible what information we collect, how we use it and what rights you have in relation to
                it.
                We hope you take some time to read through it carefully, as it is important. If there are any terms in
                this
                privacy policy that you do not agree with, please discontinue use of our Sites and our services.
            </p>
            <p>
                This privacy policy applies to all information collected through our website www.msntraveltours.com,
                and/or
                any related services, sales, marketing or events (we refer to them collectively in this privacy policy
                as
                the "Services").
            </p>
            <p>
                <strong>
                    Please read this privacy policy carefully as it will help you make informed decisions about sharing
                    your
                    personal information with us.
                </strong>
            </p>
            <h5 class="mt-5"><strong>1. WHAT INFORMATION DO WE COLLECT?</strong></h5>
            <h6><strong>Personal information you disclose to us</strong></h6>
            <p>
                <i>
                    <strong>In Short:</strong> We collect personal information that you provide to us such as name,
                    address,
                    contact
                    information,
                    and security data, payment information, and social media login data.
                </i>
            </p>
            <p>
                We collect personal information that you voluntarily provide to us when registering at the Services
                expressing an interest in obtaining information about us or our products and services, when
                participating in
                activities on the Services or otherwise contacting us.
                The personal information that we collect depends on the context of your interactions with us and the
                Services, the choices you make and the products and features you use. The personal information we
                collect
                can include the following:
            </p>
            <p>
                <strong>Publicly Available Personal Information:</strong> We collect first name, maiden name, last name,
                and
                nickname;
                passport number; current and former address; phone numbers; email addresses; and other similar data.
            </p>
            <p>
                <strong>Social Media Login Data:</strong> We provide you with the option to register using social media
                account details, like your Facebook, Google. If you choose to register in this way, we will collect the
                Information described in the section called "HOW DO WE HANDLE YOUR SOCIAL LOGINS" below.
            </p>

            <h5 class="mt-5"><strong>2. HOW DO WE USE YOUR INFORMATION?</strong></h5>
            <p><i><strong>In Short:</strong> We process your information for purposes based on legitimate business
                    interests, the fulfilment
                    of our
                    contract with you, compliance with our legal obligations, and/or your consent.</i></p>
            <p>We use personal information collected via our Services for a variety of business purposes described
                below. We
                process your personal information for these purposes in reliance on our legitimate business interests,
                in
                order
                to enter into or perform a contract with you, with your consent, and/or for compliance with our legal
                obligations. We indicate the specific processing grounds we rely on next to each purpose listed
                below.</p>

            <h6><strong>We use the information we collect or receive:</strong></h6>
            <p>1. To facilitate account creation and logon process. If you choose to link your account with us to a
                third
                party
                account (such as your Google or Facebook account), we use the information you allowed us to collect from
                those
                third parties to facilitate account creation and logon process for the performance of the contract. See
                the
                section below headed "HOW DO WE HANDLE YOUR SOCIAL LOGINS" for further information.</p>
            <p>2. To deliver services to the user. We may use your information to provide you with the requested
                service.</p>
            <p>3. To respond to user inquiries/offer support to users. We may use your information to respond to your
                inquiries
                and solve any potential issues you might have with the use of our Services.</p>

            <h5 class="mt-5"><strong>3. WILL YOUR INFORMATION BE SHARED WITH ANYONE?</strong></h5>
            <p><i><strong>In Short:</strong> We only share information with your consent, to comply with laws, to
                    provide
                    you with services, to
                    protect your rights, or to fulfill business obligations.</i></p>
            <p>We may process or share data based on the following legal basis:</p>
            <p><strong>1. Consent:</strong> We may process your data if you have given us specific consent to use your
                personal information
                in a
                specific purpose.</p>

            <p><strong>2. Legitimate Interests:</strong> We may process your data when it is reasonably necessary to
                achieve
                our legitimate
                business interests.</p>

            <p><strong>3. Performance of a Contract:</strong> Where we have entered into a contract with you, we may
                process
                your personal
                information to fulfill the terms of our contract.</p>

            <p><strong>4. Legal Obligations:</strong> We may disclose your information where we are legally required to
                do
                so in order to
                comply
                with applicable law, governmental requests, a judicial proceeding, court order, or legal process, such
                as in
                response to a court order or a subpoena (including in response to public authorities to meet national
                security
                or law enforcement requirements).</p>


            <p><strong>5. Vital Interests:</strong> We may disclose your information where we believe it is necessary to
                investigate,
                prevent, or
                take action regarding potential violations of our policies, suspected fraud, situations involving
                potential
                threats to the safety of any person and illegal activities, or as evidence in litigation in which we are
                involved.</p>
            <p>More specifically, we may need to process your data or share your personal information in the following
                situations:</p>

            <p><strong>1. Business Transfers.</strong> We may share or transfer your information in connection with, or
                during negotiations of,
                any merger, sale of company assets, financing, or acquisition of all or a portion of our business to
                another
                company.</p>
            <p><strong>2. Third-Party Advertisers.</strong> We may use third-party advertising companies to serve ads
                when
                you visit the Services.
                These companies may use information about your visits to our Website(s) and other websites that are
                contained in
                web cookies and other tracking technologies in order to provide advertisements about goods and services
                of
                interest to you.</p>

            <h5 class="mt-5"><strong>4. DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</strong></h5>
            <p><i><strong>In Short:</strong> We may use cookies and other tracking technologies to collect and store
                    your
                    information.</i></p>
            <p>
                We may use cookies and similar tracking technologies (like web beacons and pixels) to access or store
                information. Specific information about how we use such technologies and how you can refuse certain
                cookies is set out in our Cookie Policy.</p>


            <h5 class="mt-5"><strong>5. HOW DO WE HANDLE YOUR SOCIAL LOGINS?</strong></h5>
            <p><i><strong>In Short:</strong> If you choose to register or log in to our services using a social media
                    account, we may have access
                    to certain information about you.</i></p>

            <p>Our Services offer you the ability to register and login using your third party social media account
                details
                (like your Facebook or Google logins). Where you choose to do this, we will receive certain profile
                information
                about you from your social media provider. The profile Information we receive may vary depending on the
                social
                media provider concerned, but will often include your name, e-mail address, friends list, profile
                picture as
                well as other information you choose to make public.</p>
            <p>
                We will use the information we receive only for the purposes that are described in this privacy policy
                or
                that
                are otherwise made clear to you on the Services. Please note that we do not control, and are not
                responsible
                for, other uses of your personal information by your third party social media provider. We recommend
                that
                you
                review their privacy policy to understand how they collect, use and share your personal information, and
                how
                you
                can set your privacy preferences on their sites and apps.
            </p>

            <h5 class="mt-5"><strong>6. HOW LONG DO WE KEEP YOUR INFORMATION?</strong></h5>
            <p><i><strong>In Short:</strong> We keep your information for as long as necessary to fulfill the purposes
                    outlined in this
                    privacy policy unless otherwise required by law.</i></p>
            <p>We will only keep your personal information for as long as it is necessary for the purposes set out in
                this
                privacy policy, unless a longer retention period is required or permitted by law (such as tax,
                accounting or
                other legal requirements). No purpose in this policy will require us keeping your personal information
                for
                longer than the period of time in which users have an account with us.</p>
            <p>When we have no ongoing legitimate business need to process your personal information, we will either
                delete
                or
                anonymize it, or, if this is not possible (for example, because your personal information has been
                stored in
                backup archives), then we will securely store your personal information and isolate it from any further
                processing until deletion is possible.</p>

            <h5 class="mt-5"><strong>7. HOW DO WE KEEP YOUR INFORMATION SAFE?</strong></h5>
            <p><i><strong>In Short:</strong> We aim to protect your personal information through a system of
                    organizational
                    and technical security
                    measures.</i></p>
            <p>We have implemented appropriate technical and organizational security measures designed to protect the
                security
                of any personal information we process. However, please also remember that we cannot guarantee that the
                internet
                itself is 100% secure. Although we will do our best to protect your personal information, transmission
                of
                personal information to and from our Services is at your own risk. You should only access the services
                within a
                secure environment.</p>


            <h5 class="mt-5"><strong>8. DO WE COLLECT INFORMATION FROM MINORS?</strong></h5>
            <p><i><strong>In Short:</strong> We do not knowingly collect data from or market to children under 18 years
                    of
                    age.</i></p>
            <p>We do not knowingly solicit data from or market to children under 18 years of age. By using the Services,
                you
                represent that you are at least 18 or that you are the parent or guardian of such a minor and consent to
                such
                minor dependent’s use of the Services. If we learn that personal information from users less than 18
                years
                of
                age has been collected, we will deactivate the account and take reasonable measures to promptly delete
                such
                data
                from our records. If you become aware of any data we have collected from children under age 18, please
                contact
                us at msntraveltours@gmail.com.</p>

            <h5 class="mt-5"><strong>9. WHAT ARE YOUR PRIVACY RIGHTS?</strong></h5>
            <p><i><strong>In Short:</strong> In some regions, such as the European Economic Area, you have rights that
                    allow
                    you greater access to
                    and control over your personal information. You may review, change, or terminate your account at any
                    time.</i></p>
            <p>In some regions (like the European Economic Area), you have certain rights under applicable data
                protection
                laws. These may include the right (i) to request access and obtain a copy of your personal information,
                (ii)
                to
                request rectification or erasure; (iii) to restrict the processing of your personal information; and
                (iv) if
                applicable, to data portability. In certain circumstances, you may also have the right to object to the
                processing of your personal information. We will consider and act upon any request in accordance with
                applicable
                data protection laws.</p>
            <p>If we are relying on your consent to process your personal information, you have the right to withdraw
                your
                consent at any time. Please note however that this will not affect the lawfulness of the processing
                before
                its
                withdrawal.</p>
            <p>If you are resident in the European Economic Area and you believe we are unlawfully processing your
                personal
                information, you also have the right to complain to your local data protection supervisory authority. If
                you
                have questions or comments about your privacy rights, you may email us at msntraveltours@gmail.com.</p>


            <h5><strong>13. DO WE MAKE UPDATES TO THIS POLICY?</strong></h5>
            <p><i><strong>In Short:</strong> Yes, we will update this policy as necessary to stay compliant with
                    relevant
                    laws.</i></p>
            <p>We may update this privacy policy from time to time. The updated version will be indicated by an updated
                “Revised” date and the updated version will be effective as soon as it is accessible. If we make
                material
                changes to this privacy policy, we may notify you either by prominently posting a notice of such changes
                or
                by
                directly sending you a notification. We encourage you to review this privacy policy frequently to be
                informed of
                how we are protecting your information.</p>

            <h5><strong> 14. HOW CAN YOU CONTACT US ABOUT THIS POLICY?</strong></h5>
            <p>If you have questions or comments about this policy, you may email us at msntraveltours@gmail.com or by
                post
                to:</p>
            Messanger Tours and Travel<br>
            Thamel<br>
            Chaksibasri Marg<br>
            Kathmandu, Province 3 ( प्रदेशनं३ ) 44600<br>
            Nepal


        </div>

    </section>
    <!-- About Us Area End -->

@endsection
