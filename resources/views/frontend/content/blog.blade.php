@extends('frontend.layout.layout')

@section('title', 'Tours | Travel | Visit Nepal | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

    <div class="breadcrumb-area bg-img bg-overlay jarallax"
         style="background-image: url({{ asset('img/new/img5.jpg') }})">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content text-center">
                        <h2 class="page-title">Blog Section</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blog Section</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="blog-area section-padding-100-0">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8">
                    @foreach($blogs as $blog)
                        <div class="single-blog-post d-flex align-items-center mb-50">

                            <div class="post-thumbnail">
                                <a href="#"><img src="/img/thumbnail/blog/{{ $blog->image }}" alt=""></a>
                            </div>

                            <div class="post-content">
                                <div class="writer-info">
                                    <div class="writer-photo">
                                        @if($blog->writer_photo)
                                            <img src="/img/webuser/{{ $blog->writer_photo }}" alt="blog user image">
                                        @else
                                            <img src="/img/profile.jpg" alt="blog user image">
                                        @endif
                                    </div>
                                    <h5 style="color: grey; margin-right: 7vw">{{ $blog->writer }}</h5>
                                    <h6>  {{ \Carbon\Carbon::parse($blog->created_at)->diffForHumans() }}</h6>
                                </div>
                                <a href="#" class="post-title">
                                    {{ $blog->title }}
                                </a>
                                <p style="color: rgb(102,102,102) !important;">
                                    <span>{!! substr($blog->body,0,300) !!}</span>
                                </p>
                                <a href="{{ route('blog.show', $blog->slug) }}" class="btn continue-btn">Read More</a>
                            </div>
                        </div>
                    @endforeach

                    {{--Pagination--}}
                    <div class="pagination" style="margin-bottom: 5vh">
                        {{ $blogs->links() }}
                    </div>

                </div>

                <div class="col-12 col-sm-8 col-md-6 col-lg-4">
                    <div class="sidebar-area pl-md-4">

                        <!-- Recent Post -->
                        <div class="single-widget-area mb-100">
                            <h4 class="widget-title mb-30">Recent Blogs</h4>

                            <!-- Single Recent Post -->
                            @foreach($recentblogs as $recent)
                                <div class="single-recent-post d-flex">
                                    <!-- Thumb -->
                                    <div class="post-thumb">
                                        <a href="{{ route('blog.show', $recent->id) }}">
                                            <img src="/img/thumbnail/blog/{{ $recent->image }}" alt="">
                                        </a>
                                    </div>
                                    <!-- Content -->
                                    <div class="post-content">
                                        <!-- Post Meta -->
                                        <div class="post-meta">
                                            <h6 class="post-author">
                                                {{ \Carbon\Carbon::parse($recent->created_at)->diffForHumans() }}
                                            </h6>

                                        </div>
                                        <a href="{{ route('blog.show', $recent->id) }}" class="post-title"
                                           style="font-weight: bold;">
                                            {!! substr($recent->title,0,30) !!}
                                        </a>
                                    </div>
                                </div>
                            @endforeach

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Blog Area End -->

@endsection
