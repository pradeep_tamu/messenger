@extends('frontend.layout.layout')

@section('title'){{substr($blog->title,0,20)}}@endsection
@section('description'){substr($blog->body,0,150)}}@endsection
@section('keywords', 'travel blogs,travel nepal,visit nepal 2020')

@section('content')
    <div class="breadcrumb-area bg-img bg-overlay jarallax"
         style="background-image: url({{ asset('img/new/img8.jpg') }});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content text-center">
                        <h1 class="page-title">Blog Section</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="{{ Route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active">Blog</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Blog Area Start -->
    <div class="blog-area section-padding-100-0">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8">
                    <div class="single-writer-info">
                        <div class="single-writer-photo">
                            <img
                                src="@if($blog->writer_photo ){{ asset('/img/user/'. $blog->writer_photo) }} @else {{ asset('img/profile.jpg')}} @endif"
                                alt="">
                        </div>
                        <div class="single-writer-desc">
                            <h4>{{ $blog->writer }}</h4>
                            <h6>  {{ \Carbon\Carbon::parse($blog->created_at)->diffForHumans() }}</h6>
                        </div>
                    </div>

                    <!-- Post Thumbnail -->
                    <div class="post-thumbnail">
                        <img src="/img/blog/{{ $blog->image }}" alt="img">
                    </div>
                    <div class="blog-title">
                        <h2>
                            {{$blog->title}}
                        </h2>
                    </div>
                    <!-- Blog Details Text -->
                    <div class="blog-details-text">
                        {!! $blog->body !!}
                    </div>
                    <!-- Comments Area -->
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="sidebar-area pl-md-4">

                        <div class="single-widget-area mb-100">
                            <h4 class="widget-title mb-30">Recent Blogs</h4>

                            <!-- Single Recent Post -->
                            @foreach($recentblogs as $recent)
                                <div class="single-recent-post d-flex">
                                    <!-- Thumb -->
                                    <div class="post-thumb">
                                        <a href="{{ route('blog.show', $recent->id) }}">
                                            <img src="/img/thumbnail/blog/{{ $recent->image }}" alt="">
                                        </a>
                                    </div>
                                    <!-- Content -->
                                    <div class="post-content">
                                        <!-- Post Meta -->
                                        <div class="post-meta">
                                            <a href="#" class="post-author">
                                                {{ \Carbon\Carbon::parse($recent->created_at)->diffForHumans() }}
                                            </a>

                                        </div>
                                        <a href="{{ route('blog.show', $recent->id) }}"
                                           class="post-title font-weight-bold">
                                            {!! substr($recent->title,0,30) !!}
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
