@extends('frontend.layout.layout')

@section('title'){{$package->meta_title}}@endsection
@section('description'){{$package->meta_description}}@endsection
@section('keywords'){{$package->meta_keywords}}@endsection

@section('content')
    <div class="landing-slides owl-carousel">
        <!-- Single Welcome Slide -->
        <div class="single-landing-slide bg-overlay package-slide" data-img-url="img/new/img3.jpg"
             style="overflow: hidden;">
            <img
                src="@if($package->filename ){{ asset('/img/package/'. $package->filename) }} @else {{ asset('img/profile.jpg')}} @endif"
                alt="" style="height: 100%;">

        </div>

    </div>


    <div class="tour-name container">
        <div class="cost-tag">
            <div class="price-tag-vertical center"><span>$ {{$package->cost}}</span></div>
        </div>
        <h2>{{$package->name}}</h2>

    </div>

    <div class="package-area container">
        <div class="row">
            <div class="col-12 col-lg-7 col-xl-8">
                <ul class="nav d-flex text-bold" style="margin-bottom: 5%;">
                    <li class="active"><a data-toggle="tab" href="#Overview"
                                          class="text-info show active">Overview</a></li>
                    @if($package->itineraries != "<p>no</p>")
                        <li><a data-toggle="tab" href="#Itineraries" class="text-info">Itineraries</a></li>
                    @endif
                    <li><a data-toggle="tab" href="#Cost" class="text-info">Cost Details</a></li>
                    <li><a data-toggle="tab" href="#Practical" class="text-info">Practical Information</a></li>
                    <li><a data-toggle="tab" href="#dates" class="text-info">Dates & Discounts</a></li>
                </ul>

                <div class="tab-content listing">
                    <div id="Overview" class="tab-pane fade active show">

                        <div class="table-responsive panel">
                            {!! $package->overview !!}
                        </div>
                    </div>
                    @if($package->itineraries != "<p>no</p>")

                        <div id="Itineraries" class="tab-pane fade">
                            <div class="table-responsive panel">
                                {!! $package->itineraries !!}
                            </div>
                        </div>
                    @endif

                    <div id="Cost" class="tab-pane fade">
                        <div class="table-responsive panel">
                            {!! $package->cost_details !!}
                        </div>
                    </div>
                    <div id="Practical" class="tab-pane fade">
                        <div class="table-responsive panel">
                            {!! $package->practical_info !!}
                        </div>
                    </div>
                    <div id="dates" class="tab-pane fade">
                        <div class="table-responsive panel">
                            <table class="table table-bordered">
                                <thead>
                                <tr class="table-active">
                                    <th scope="col">Year</th>
                                    <th scope="col">Cost</th>
                                    <th scope="col">Discount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="table-danger">
                                    <th scope="row">2020</th>
                                    <td>${{$package->cost}}</td>
                                    <td>18% each</td>
                                </tr>
                                <tr class="table-secondary">
                                    <th scope="row">2021</th>
                                    <td>${{$package->cost}}</td>
                                    <td>15% each</td>
                                </tr>
                                <tr class="table-success">
                                    <th scope="row">2022</th>
                                    <td>${{$package->cost}}</td>
                                    <td>12% each</td>
                                </tr>
                                <tr class="table-primary">
                                    <th scope="row">2023</th>
                                    <td>${{$package->cost}}</td>
                                    <td>9% each</td>
                                </tr>
                                <tr class="table-secondary">
                                    <th scope="row">2024</th>
                                    <td>${{$package->cost}}</td>
                                    <td>6% each</td>
                                </tr>
                                <tr class="table-info">
                                    <th scope="row">2025</th>
                                    <td>${{$package->cost}}</td>
                                    <td>3% each</td>
                                </tr>
                                </tbody>
                            </table>

                            <p><b>Note:</b>There will be 2(percent) additional discount per person, if the group of
                                people exceeds the minimum group of people ({{$package->capacity}})</p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-12 col-lg-5 col-xl-4">
                <!-- Hotel Reservation Area -->
                <div class="choose-us"><h2>Why Book With Us?</h2>
                    <ul class="list">
                        <li><i class="fas fa-dollar-sign"></i>No-hassle
                            best price guarantee
                        </li>
                        <li><i class="fa fa-headphones"
                            ></i>Customer care available
                            24/7
                        </li>
                        <li><i class="fa fa-star"></i>Hand-picked
                            Tours &amp; Activities
                        </li>
                        <li><i class="fas fa-ambulance"></i>Adequate
                            Travel Insurance
                        </li>
                    </ul>
                </div>
                <div class="book-now">
                    <a href="{{route('booking',["type"=> 'package', "slug"=>$package->slug])}}">
                        <button>Book Now</button>
                    </a>
                    <a href="{{route('contact')}}">
                        <button>Enquiry</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="comment_area mb-50 clearfix container">
        @if(count($reviews) !=0)
            <h4 style="margin: 4vh 0">
                @for($i=1;$i<=$package->averageRating();$i++)
                    <i class="fas fa-star active_star"></i>
                @endfor
                @for($i=$package->averageRating();$i<5;$i++)
                    <i class="fas fa-star"></i>
                @endfor
                {{ $package->ratingPercent(5) }}%
            </h4>

            <h2 style="margin: 0; font-weight: bolder">User Reviews and Comments</h2>
        @endif
        <hr>

        <!-- Single Comment Area -->
        @foreach($reviews as $review)
            <div class="single_comment_area">
                <!-- Comment Content -->
                <div class="comment-content d-flex">
                    <!-- Comment Author -->
                    <div class="comment-author">
                        <img src="/storage/webusers/{{ $review->image }}" alt="author">
                    </div>
                    <!-- Comment Meta -->
                    <div class="comment-meta col-md-10">
                        <a href="#"
                           class="post-date">{{ \Carbon\Carbon::parse($review->created_at)->diffForHumans() }}</a>
                        <h5>{{ $review->writer }}</h5>
                        @if(Auth::check())
                            @if($review->writer_id == Auth::user()->id)
                                <form action="{{ route('package.review.edit', $review->id) }}" method="post">
                                    @csrf
                                    <textarea class="form-control col-md-9"
                                              name="review">{{ $review->review }}</textarea>
                                    <h6>
                                        @foreach($ratings as $rating)
                                            @if($rating->user_id == $review->writer_id)
                                                @for($i=1;$i<=$rating->rating;$i++)
                                                    <i class="fas fa-star active_star"></i>
                                                @endfor
                                                @for($i=$rating->rating;$i<5;$i++)
                                                    <i class="fas fa-star"></i>
                                                @endfor
                                            @endif
                                        @endforeach
                                    </h6>
                                    <button type="submit" class="btn btn-sm btn-outline-success">Edit Your Post</button>
                                </form>
                            @else
                                <p>{{ $review->review }}</p>
                            @endif
                        @else
                            <p>{{ $review->review }}</p>
                        @endif

                    </div>
                </div>
            </div>
        @endforeach
        @if(count($reviews) !=0)
            <button class="see-more">View More...</button>
            <button class="see-less">View Less...</button>
    @endif
    <!-- End Single Comment Area -->
        <h2 style="margin: 0; font-weight: bolder">Write Your Review</h2>
        <hr>

        <div class="comment-author" style="flex-direction: column;">
            @if(Auth::check())
                <form action="{{ route('package.review') }}" method="post">
                    @csrf
                    <input name="package_id" value="{{ $package->id }}" type="hidden">
                    <div class="row rating">
                        <div class="col-md-4" style="font-size: 35px; margin-left: 1vh">
                            <label>
                                <input type="radio" name="rating" value="1"/>
                                <span class="icon">★</span>
                            </label>
                            <label>
                                <input type="radio" name="rating" value="2"/>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                            </label>
                            <label>
                                <input type="radio" name="rating" value="3"/>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                            </label>
                            <label>
                                <input type="radio" name="rating" value="4"/>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                            </label>
                            <label>
                                <input type="radio" name="rating" value="5"/>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                            </label>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-10">
                            <div class="row active_review_box">
                                <img src="/storage/webusers/{{ Auth::user()->image }}" alt="author"
                                     style="margin-right: 2vh">
                                <textarea id="comment" class="form-control col-md-10" placeholder="Write your Review"
                                          name="review" style="width: 75%"></textarea>
                                <span class="invalid-feedback d-block">{{ $errors->first('review') }}</span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-secondary">Submit your Review</button>
                        </div>
                    </div>
                </form>
            @else
                <span style="color: #0080ff">Please <a href="{{ route('login') }}"style="color: #0000F0; text-decoration: underline">
                        Login</a> to post a review... </span>
                <textarea class="form-control col-md-12" name="review" placeholder="Write your Review"
                          disabled></textarea>
            @endif
        </div>

    </div>

    <!-- Review Area -->


    <div class="tour-name container related-package">

        <h2>Related Packages</h2>
    </div>

    <div class="package-slide-img container mb-5">
        <div class="row">
            @foreach($related_package as $package)
                <div class="img-section col-12 col-md-4">
                    <div class="slide-img">
                        <a href="{{route('package.show', $package->slug)}}">
                            <img
                                src="@if($package->filename ){{ asset('img/thumbnail/package/'. $package->filename) }} @else {{ asset('img/profile.jpg')}} @endif"
                                alt="" style="height: 100%;">
                        </a>
                    </div>
                    <div class="pacakage-desc">
                        <div class="title-desc">
                            <div class="desc-title">{{$package->name}}</div>
                            <div class="price">
                                <div class="discount">
                                    From
                                </div>
                                <div class="actual-price">
                                    ${{$package->cost}}
                                </div>
                            </div>
                        </div>
                        <div class="review " style="padding-left: 1vh">
                            <h6>
                                @for($i=1;$i<=$package->averageRating();$i++)
                                    <i class="fas fa-star active_star"></i>
                                @endfor
                                @for($i=$package->averageRating();$i<5;$i++)
                                    <i class="fas fa-star"></i>
                                @endfor
                                {{ $package->ratingPercent(5) }}%
                            </h6>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>

@endsection
