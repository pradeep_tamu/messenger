@extends('frontend.layout.layout')

@section('title', 'Tours | Travel | Visit Nepal | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

    <!-- Breadcrumb Area Start -->
    <div class="breadcrumb-area bg-img bg-overlay jarallax"
         style="background-image: url({{ asset('img/new/img5.jpg') }});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content text-center">
                        <h1 class="page-title">Packages</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="{{Route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active">Packages</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- search -->
    <div class="container mt-5">
        <div class="box">
            <h3>Search your package</h3>

            <div class="form-group col-md-8 p-0">
                <input type="text" name="search-keyword" id="search-package" class="w-50 form-control input-lg"
                       placeholder="Enter Any Keyword"/>
            </div>
            <br/>
            {{ csrf_field() }}
        </div>
    </div>

    <!-- packages -->
    <div class="packages-area ">
        <div class="container p-0">
            <div class="row">

                <div class="col-12 col-lg-8 ">
                    <div id="search-result">
                    </div>
                </div>

                <div class="container col-12 col-lg-12 col-xl-8" id="all-package-list">

                    @if(count($packages) == 0)
                        <div class="message-box">
                            There are no Packages in this destination
                        </div>
                    @endif
                <!-- Single Room Area -->
                    @foreach($packages as $package)
                        <div class="single-package-area d-flex mb-50">
                            <!-- Room Thumbnail -->
                            <div class="package-thumbnail">
                                <img src="{{ asset('img/thumbnail/package/'. $package->filename) }}"
                                     alt="package image">
                            </div>
                            <!-- Room Content -->
                            <div class="package-content">
                                <h2>{{$package->name}}</h2>
                                <span>From <h4>$ {{$package->cost}}</h4></span>
                                <div class="package-feature">
                                    <h6>Duration: <span>{{$package->duration}}</span></h6>
                                    <h6>Capacity: <span>{{$package->capacity}}</span></h6>
                                    <h6 class="desc">Description: <span class="desc"
                                                                        style="width:100%">{!! substr($package->description,0,120) !!}</span>
                                    </h6>
                                </div>
                                <button><a href="{{ Route('package.show',$package->slug) }}"
                                           class="btn view-detail-btn">View
                                        Details <i class="fas fa-long-arrow-alt-right"></i></a>
                                </button>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="col-12 col-xl-4">
                    <!-- Hotel Reservation Area -->
                    <div class="choose-us"><h2>Why Book With Us?</h2>
                        <ul class="list">
                            <li><i class="fas fa-dollar-sign"></i>No-hassle
                                best price guarantee
                            </li>
                            <li><i class="fa fa-headphones"
                                ></i>Customer care available
                                24/7
                            </li>
                            <li><i class="fa fa-star"></i>Hand-picked
                                Tours &amp; Activities
                            </li>
                            <li><i class="fas fa-ambulance"></i>Adequate
                                Travel Insurance
                            </li>
                        </ul>
                    </div>
                    <div>
                        <iframe
                            src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fmsntraveltours%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1050834215283219"
                            width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                            allowTransparency="true" allow="encrypted-media"></iframe>
                    </div>
                </div>
            </div>
            <!-- Pagination -->
            <span class="pagination" style="margin-bottom: 5vh;">
                {{ $packages->links() }}
        </span>
        </div>
    </div>
    <!-- Rooms Area End -->

@endsection
