@extends('frontend.layout.layout')

@section('title', 'Tours | Travel | Visit Nepal | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

    <div class="breadcrumb-area bg-img bg-overlay jarallax"
         style="background-image: url({{ asset('img/new/img8.jpg') }});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content text-center">
                        <h1 class="page-title">Destinations</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="{{Route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active">Destinations</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- search -->
    <div class="container search-container mt-5">
        <div class="box destination-box">
            <h3>Search your destination</h3>

            <div class="form-group col-md-8 p-0">
                <input type="text" name="search-keyword" id="search-destination" class=" search form-control input-lg"
                       placeholder="Enter Any Keyword"/>
            </div>
            <br/>
            {{ csrf_field() }}
        </div>
    </div>

    <div class="destinations container">
        <div class="row" id="search-result">

        </div>
        <div class="row" id="all-destination-list">
            @foreach($destinations as $destination)
                <div class="single-project-slide active bg-img col-12 col-md-6 col-lg-4">
                    <div class="project-content">
                        <img src="{{ asset('/img/destination/'. $destination->filename) }} "
                             alt="destination in nepal image">
                    </div>
                    <!-- Hover Effects -->
                    <div class="hover-effects">
                        <div class="text">
                            <h3>{{$destination->name}}</h3>
                            <p> {!! substr($destination->description,0,350) !!}</p>
                        </div>
                        <a href="{{ Route('destination.package',$destination->slug) }}" class="btn project-btn">Discover
                            Now <i class="fa fa-long-arrow-right"
                                   aria-hidden="true"></i></a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
