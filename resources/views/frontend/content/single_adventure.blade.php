@extends('frontend.layout.layout')

@section('title'){{$adventure->meta_title}}@endsection
@section('description'){{$adventure->meta_description}}@endsection
@section('keywords'){{$adventure->meta_keywords}}@endsection

@section('content')

    <div class="adventure-display">
        <img
            src="@if($adventure->filename ){{ asset('/img/adventure/'. $adventure->filename) }} @else {{ asset('img/profile.jpg')}} @endif"
            alt="adventure image">
    </div>

    <div class="adventure-name container mt-5">
        <h1>{{$adventure->name}}</h1>
    </div>

    <div class="single-adventure ">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-12 text-justify">

                    <span>{!! $adventure->description !!} </span>
                    <table class="table table-striped" style="margin-top: 2rem">

                        <tr>
                            <th> Location</th>
                            <td>{{$adventure->location}}</td>
                        </tr>
                        <tr>
                            <th>Activity</th>
                            <td>{{$adventure->name}}</td>
                        </tr>
                        <tr>
                            <th>Cost</th>
                            <td>${{$adventure->cost}}</td>
                        </tr>
                        <tr>
                            <th>Videos</th>
                            <td>{{$adventure->videos}}</td>
                        </tr>
                        <tr>
                            <th>Season</th>
                            <td>{{$adventure->season}}</td>
                        </tr>
                        <tr>
                            <th>Transportation</th>
                            <td>{{$adventure->transportation}}</td>
                        </tr>
                        <tr>
                            <th>Trip starts/ends</th>
                            <td>{{$adventure->start_end}}</td>
                        </tr>

                    </table>

                </div>
                <div class="col-12 col-lg-4">
                    <!-- Hotel Reservation Area -->
                    <div class="choose-us"><h2>Why Book With Us?</h2>
                        <ul class="list">
                            <li><i class="fas fa-dollar-sign"></i>No-hassle
                                best price guarantee
                            </li>
                            <li><i class="fa fa-headphones"
                                ></i>Customer care available
                                24/7
                            </li>
                            <li><i class="fa fa-star"></i>Hand-picked
                                Tours &amp; Activities
                            </li>
                            <li><i class="fas fa-ambulance"></i>Adequate
                                Travel Insurance
                            </li>
                        </ul>
                    </div>
                    <div class="book-now">
                        <a href="{{route('booking',["type"=> 'adventure', "slug"=>$adventure->slug])}}">
                            <button>Book Now</button>
                        </a>

                        <a href="{{route('contact')}}">
                            <button>Enquiry</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="tour-name container mt-5">
        <h2>Popular Adventures</h2>
    </div>

    <div class="package-slide-img container mb-5">
        <div class="row">
            @foreach($related_adventure as $adventure)
                <div class="img-section col-sm-4">
                    <div class="slide-img" style="height: 250px">
                        <a href="{{route('adventure.show', $adventure->slug)}}">
                            <img
                                src="@if($adventure->filename ){{ asset('/img/thumbnail/adventure/'. $adventure->filename) }} @else {{ asset('img/profile.jpg')}} @endif"
                                alt="" style="height: 100%;">
                        </a>
                    </div>
                    <div class="pacakage-desc">
                        <div class="title-desc">
                            <div class="desc-title">{{$adventure->name}}</div>
                            <div class="price">
                                <div class="discount">
                                    From
                                </div>
                                <div class="actual-price">
                                    $ {{$adventure->cost}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            @endforeach


        </div>
    </div>

@endsection
