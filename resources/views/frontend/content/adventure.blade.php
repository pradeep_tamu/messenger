@extends('frontend.layout.layout')

@section('title', 'Tours | Travel | Visit Nepal | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

    <!-- Breadcrumb Area Start -->
    <div class="breadcrumb-area bg-img bg-overlay jarallax"
         style="background-image: url({{ asset('img/adventure.jpeg') }});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content text-center">
                        <h2 class="page-title">Adventures</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Adventures</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End -->

    <!-- search -->
    <div class="container search-container mt-5">
        <div class="box">
            <h3>Search your adventure</h3>

            <div class="form-group col-md-8 p-0">
                <input type="text" name="search-keyword" id="search-adventure" class="search form-control input-lg"
                       placeholder="Enter Any Keyword"/>
            </div>
            <br/>
            {{ csrf_field() }}
        </div>
    </div>

    <div class="adventure-area container">
        <div class="p-0">

            <div class="row" id="search-result">

            </div>

            <div class="row" id="all-adventures-list">
                @foreach($adventures as $adventure)
                    <div class="col-12 col-lg-9">
                        <!-- Single Room Area -->
                        <div class="single-adventure-area d-flex align-items-center mb-50">
                            <!-- Room Thumbnail -->
                            <div class="adventure-thumbnail">
                                <img
                                    src="@if($adventure->filename ){{ asset('/img/thumbnail/adventure/'. $adventure->filename) }} @else {{ asset('img/profile.jpg')}} @endif"
                                    alt="adventure image">

                            </div>
                            <!-- Room Content -->
                            <div class="adventure-content">
                                <h2>{{$adventure->name}}</h2>
                                <h5 class="desc">Description: <span class="desc"
                                                                    style="width:100%">{!! substr($adventure->description,0,300) !!}</span>
                                </h5>
                            </div>
                        </div>

                    </div>
                    <div class="details col-12 col-lg-3">
                        <h2>From</h2>
                        <h4>$ {{$adventure->cost}}</h4>
                        <a href="{{Route('adventure.show', $adventure->slug)}}">
                            <button>View Details <i class="fas fa-long-arrow-alt-right"></i></button>
                        </a>
                    </div>
                @endforeach
            </div>
            <!-- Pagination -->
            <span class="pagination" style="margin-bottom: 5vh;">
                {{ $adventures->links() }}
        </span>
        </div>
    </div>

@endsection
