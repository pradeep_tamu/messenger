@extends('frontend.layout.layout')

@section('title', 'Tours | Travel | Visit Nepal | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

    <section class="privacy mt-5 mb-5">
        <div class="privacy-wrapper container">
            <h3><strong>Discover Bhutan (The Last Shangri-La): </strong></h3>
            <p>A Himalayan kingdom next to Nepal is still perhaps the world's most exclusive tourist on the earth
                reflecting
                age-old religion, culture, arts and architecture in its golden peak in the background of deep green
                valley
                and hillside. People are deeply religious following the Mahayana form of Buddhism. The air of
                spirituality
                is evident, even in the urban centres where the spinning of prayer wheels, the murmur of mantras and the
                glow of butter lamp in the houses are still important feature of everyday life. Monastery, temples and
                religious monuments are dotted across the landscape, bearing witness to the importance of Buddhism.</p>

            <h5 class="mb-3"><strong>Bhutan at a Glance:</strong></h5>
            <span><strong>Area:</strong> 47,000 sq. km</span><br>
            <span><strong>Capital:</strong> Thimphu</span><br>
            <span><strong>Continent:</strong> South Asia</span><br>
            <span><strong>Seasons:</strong> Summer, autumn, winter, and spring</span><br>
            <span><strong>Language:</strong> Dzongkha & English</span><br>
            <span><strong>Currency:</strong>  Ngultrum</span><br>
            <span><strong>Population:</strong> (2006 approx.) 2,279,723</span><br>
            <span><strong>Religions:</strong> Buddhist & Hinduism</span>


            <p>Cotton clothes are sufficient from May until September but warm clothes are very much necessary from
                November
                to
                the end of April. However, visitors are advised to carry clothes consisting of layers (or preferably
                woollen
                sweater and jacket) throughout the season, as weather may change at any time.</p>
            <p>The Royal government of Bhutan categories all hotels and lodges with a unique combination of tradition
                Bhutanese
                style and modern services. Clean and well-maintained rooms, most of the hotels are equipped with
                telephone,
                fax
                machines, restaurants etc. Even some of the simple lodges have a special charm of their own, and the
                guests
                are
                treated with the traditional warm hospitality.Weather conditions are ideal for visiting Bhutan in all
                seasons.
                Temperatures in the south are tropical, with high monsoon rainfall. Rainfall in the higher central
                region is
                not
                so heavy. The days are clear and warm with cool nights.Normal clothing preferably cotton for summer and
                woollen
                clothing for evening and winter. For trekking strong comfortable trekking boots, warm socks, sunglasses,
                head-gear, rain coat and warm clothing, including a down jacket for higher altitude treks. A medium
                sized
                sleeping bag is a must.</p>


            <h3><strong>Getting to Bhutan</strong></h3>
            <p>Druk Air and Bhutan Airlines are the only two Bhutanese Airlines that operate flights to Bhutan.</p>

            <p><strong>Druk Air –</strong> Royal Bhutan Airlines is the National Flag carrier of the kingdom of Bhutan
                and
                operates 4 (3- A319)
                and 1 ATR 42-500) series Aircrafts and flies to and from Singapore, (Kolkata, New Delhi, Gaya, Mumbai,
                Bagdogra
                and Gauhati in India), Bangkok (Thailand), Kathmandu (Nepal). Druk Air also flies to domestic
                destinations
                Yonphula, Gelephu and Bumthang.</p>
            <p><strong> Bhutan Airlines:</strong> A private Airline operates 2 A319 aircrafts with a seating capacity of
                126
                passengers, flies to
                Bangkok via Kolkata on daily basis and 3 flights to Delhi, India via Kathmandu.</p>

            <h5><strong>Traveling to Bhutan by Road</strong></h5>
            <p>For traveling to Bhutan by road, you have to enter Bhutan from three Southern districts of Phuentsholing
                which
                shares border with Indian border town of Jaigaon (West Bengal), Gelephu and Samdrup Jongkhar share
                border
                with
                the Indian state of Assam. The nearest airport is at Bagdogra (174 km – 4 hour drive) and you can
                combine
                traveling to Bhutan after or before your Darjeeling and Sikkim trip in Northern East India.</p>

            <p>Badrapur (about 5 hours’ drive from Phuentsholing) and Birat Nagar (about 7 hours’ drive from
                Phuentsholing)
                are
                two other nearby airports in Nepal, connected with regular flights from Kathmandu.</p>


            <h3><strong>Bhutan Travel Visa</strong></h3>
            <p>All foreign nationals other than Indian nationals in groups of four or more may visit Bhutan.
                Applications
                for Visa should be made at least three weeks in advance. Visa fee US$ 50 needs to be paid at entry point
                (Paro airport and Phuntsholing) along with 2 passport photos with your passport number in the back.</p>
        </div>
    </section>

@endsection
