@extends('frontend.layout.layout')

@section('title', 'Tours | Travel | Visit Nepal | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

    <!-- Breadcrumb Area Start -->
    <div class="breadcrumb-area bg-img bg-overlay jarallax"
         style="background-image: url({{ asset('img/new/img8.jpg') }});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content text-center">
                        <h2 class="page-title">About Us</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">About Us</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End -->

    <!-- About Us Area Start -->
    <section class="about-us-area section-padding-100-0">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <!-- Section Heading -->
                    <div class="row">
                        <div class="col-12 col-md-8 about-p-tag">
                            <div class="section-heading">
                                <h2>Who Are We?</h2>
                            </div>
                            <p>
                                Messanger Tours and Travel was founded by a team of professional trekking and climbing
                                guides with a decade long experience in adventure tourism in the Himalaya. It is
                                officially
                                registered with the Ministry of Tourism, Government of Nepal and associated to different
                                National tourism related organizations such as Trekking Agencies Association of Nepal
                                (TAAN), Nepal Mountaineering Association (NMA), Nepal Tourism Board (NTB) and Kathmandu
                                Environmental Educational Project (KEEP).We are located at central tourist hub of
                                Kathmandu
                                that is Thamel.
                            </p>

                            <p>
                                We specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                                tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                                paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                                to
                                Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                                well-researched and developed to suit any particular interest and requirement of our
                                clients.
                            </p>

                            <p>
                                The company has its own reputation in the local as well as global tourism market for its
                                professionalism, reliability and quality services, competitive prices, knowledgeable and
                                experienced trekking/climbing guides, multi-lingual tour guides and superb
                                trekking/climbing
                                equipment. We ensure 100% safety to all our clients during the trip. We are not only
                                after
                                ‘financial gain’ like other competitors but are guided by our own business ethic for how
                                to
                                run all the trips in eco-friendly way and make our clients happy as well as satisfied.
                                We
                                offer all the logistical support to our clients whether they come in a small group, as a
                                family, or as an individual.
                            </p>

                            <h3>Why Book with Us</h3>

                            1. A local based company:

                            <p>
                                A truly locally based eco- friendly and responsible adventure travel company
                                specializing in
                                organizing various adventure, culture and heritage tours or small groups, families
                                and
                                individuals throughout Nepal, Tibet and Bhutan, at the most competitive rates.
                            </p>

                            2. Professional and trained team:
                            <p>Our team is comprised of travel experts, multi-lingual tour leaders, government-
                                licensed
                                professional trekking/climbing guides and porters having a vast experience within the
                                tourism sector. They have a wealth of hands-on knowledge about geography, flora and
                                fauna in
                                each of the Himalayan regions. They know the culture, history, religion, lifestyle of
                                the
                                people in and out.</p>

                            <p>Our outdoor staffs are well-trained and have the ability to cope with most emergency
                                situations including those requiring evacuation and hospitalization during a trip, in a
                                worst-case-scenario. Our staffs are well-paid and insured. Adequate clothing is provided
                                to
                                porters for their protection in bad weather and at higher altitudes. In case porters or
                                guides fall sick or get injured during a trip, they are given proper medical care in
                                hospital. Sufficient funds have been allocated to cover cost of their rescue and
                                treatment.</p>

                            3. Support local economy:
                            <p>We are not only dedicated to promote a genuine spirit of adventure and leisure travel
                                activities among the travellers but also foster a long-lasting positive impact on the
                                local
                                culture and environment. We take a holistic grassroots approach to travel using
                                exclusively
                                local guides/porters, family run lodges/hotels, sampling local dishes and using the
                                local
                                transportation infrastructure.</p>

                            <p>We are an adventure travel company with a conscience, and hence, profit is not only our
                                sole
                                aim or achievement. We believe in helping the remote communities and where possible,
                                make an
                                effort to support rural schools and health centre. Each year we allocate a part of our
                                profits for charitable funds.</p>

                            4. Positive reviews:
                            <p>We receive many positive reviews about our services online from our past travellers. We
                                do
                                everything possible to make sure that all our clients return home with great memories
                                about
                                the places they visited and the services we provided them with during their trip.</p>

                            5. Customized Itineraries:
                            <p> Each of our tour packages on trip advisor is specially designed to promote the Himalayan
                                destinations. We're able to customize and personalize itineraries considering interests,
                                timeframe and budget. We provide any hotels, most modes of transport and have
                                multi-lingual
                                tour guides in most languages.</p>

                            6. Our client’s safety is always our top priority:
                            <p> We constantly keep in touch with our guides who provide us with updates as well. In case
                                any
                                problems arise, we try our best to solve them as effectively and quick as possible. We
                                want
                                every traveller to return safely.</p>


                        </div>
                        <div class="col-12 col-md-4">
                            <iframe
                                src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fmsntraveltours%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1050834215283219"
                                width="340" height="400" style="border:none;overflow:hidden" scrolling="no"
                                frameborder="0"
                                allowTransparency="true" allow="encrypted-media"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Us Area End -->

    <!-- Video Area Start -->
    <div class="board-area bg-img bg-overlay jarallax" style="background-image: url({{ asset('img/new/img5.jpg') }});">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center">
                <div class="col-12 col-md-6">
                    <!-- Section Heading -->
                    <div class="section-heading text-center white ">
                        <h6>Ultimate Solutions</h6>
                        <h2>You Will Never Walk Alone</h2>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Video Area End -->

    <!-- Service Area Start -->
    <section class="service-area section-padding-100" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading -->
                    <div class="section-heading text-center ">
                        <h2>Our Services</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Service Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-service-area mb-100">
                        <img src="img/new/trek.jpg" alt="">
                        <div class="service-title d-flex align-items-center justify-content-center">
                            <h5>Trekking In Nepal</h5>
                        </div>
                    </div>
                </div>
                <!-- Single Service Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-service-area mb-100">
                        <img src="img/new/adventure.jpg" alt="">
                        <div class="service-title d-flex align-items-center justify-content-center">
                            <h5>Feel the Adventure</h5>
                        </div>
                    </div>
                </div>
                <!-- Single Service Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-service-area mb-100">
                        <img src="img/new/hotel.jpg" alt="">
                        <div class="service-title d-flex align-items-center justify-content-center">
                            <h5>Hotels</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Service Area End -->

@endsection
