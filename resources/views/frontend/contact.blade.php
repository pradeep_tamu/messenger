@extends('frontend.layout.layout')

@section('title', 'Tours | Travel | Visit Nepal | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

    <div class="breadcrumb-area contact-breadcrumb bg-img bg-overlay jarallax"
         style="background-image: url({{ asset('img/new/img5.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-content text-center mt-100">
                        <h2 class="page-title">Contact Us</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="google-maps-contact-info">
        <div class="container-fluid">
            <div class="google-maps-contact-content">
                <div class="row">
                    <!-- Single Contact Info -->
                    <div class="col-6 col-lg-3">
                        <div class="single-contact-info">
                            <i class="fa fa-phone-alt icon-color" aria-hidden="true"></i>
                            <h4>Phone</h4>
                            <p>(+977) 9851214726</p>
                            <p> 01 4412899</p>
                        </div>
                    </div>
                    <!-- Single Contact Info -->
                    <div class="col-6 col-lg-3">
                        <div class="single-contact-info">
                            <i class="fa fa-map-marker-alt icon-color" aria-hidden="true"></i>
                            <h4>Address</h4>
                            <p>Chaksibasri Marg, Thamel, Kathamndu, Nepal</p>
                        </div>
                    </div>
                    <!-- Single Contact Info -->
                    <div class="col-6 col-lg-3">
                        <div class="single-contact-info">
                            <i class="fas fa-clock icon-color" aria-hidden="true"></i>
                            <h4>Open time</h4>
                            <p>10:00 am to 6:00 pm</p>
                        </div>
                    </div>
                    <!-- Single Contact Info -->
                    <div class="col-6 col-lg-3">
                        <div class="single-contact-info">
                            <i class="fas fa-envelope icon-color" aria-hidden="true"></i>
                            <h4>Email</h4>
                            <p>msntraveltours@gmail.com</p>
                        </div>
                    </div>
                </div>
                <!-- Google Maps -->
                <div class="google-maps">
                    <iframe width="600" height="500" id="gmap_canvas"
                            src="https://maps.google.com/maps?q=Messenger%20Tours%20and%20Travel&t=&z=13&ie=UTF8&iwloc=&output=embed"
                            frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                    </iframe>
                </div>
            </div>
        </div>
    </section>

    <div class="contact-form-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading -->
                    <div class="section-heading text-center ">
                        <h5 class="icon-color text-uppercase">Have a question?</h5>
                        <h2>Leave Message</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <!-- Form -->
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div class="contact-form">
                        <form action="{{ route('enquiry') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-lg-6 ">
                                    <input type="text"
                                           name="name"
                                           class="form-control mb-30 @if($errors->first('name')) is-invalid @endif"
                                           placeholder="Name"
                                           required>
                                    <span class="invalid-feedback d-block"
                                          role="alert">{{ $errors->first('name') }}</span>
                                </div>
                                <div class="col-12 col-lg-6 ">
                                    <input type="email"
                                           name="email"
                                           class="form-control mb-30 @if($errors->first('email')) is-invalid @endif"
                                           placeholder="Email Address"
                                           required>
                                    <span class="invalid-feedback d-block"
                                          role="alert">{{ $errors->first('email') }}</span>
                                </div>
                                <div class="col-12">
                                    <input type="text"
                                           name="subject"
                                           class="form-control mb-30"
                                           placeholder="Subject @if($errors->first('subject')) is-invalid @endif"
                                           required>
                                    <span class="invalid-feedback d-block"
                                          role="alert">{{ $errors->first('subject') }}</span>
                                </div>
                                <div class="col-12 ">
                                    <textarea name="message"
                                              class="form-control mb-30 @if($errors->first('message')) is-invalid @endif"
                                              placeholder="Message"
                                              required></textarea>
                                    <span class="invalid-feedback d-block"
                                          role="alert">{{ $errors->first('message') }}</span>
                                </div>
                                <div class="col-12 text-center ">
                                    <button type="submit" class="btn landing-btn mt-15 icon-bg">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
