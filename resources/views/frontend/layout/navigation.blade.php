<header class="web-navigation">
    <div class="top-navigation container">
        <div class="top-logo">
            <a href="{{route('home')}}"> <img src="{{ asset('img/logo.png') }}" alt="no image"></a>
        </div>
        <div class="support">
            <i class="fas fa-phone-alt icon-color"></i>
            <div class="support-details">
                <p>24/7 Support</p>
                <p>+ 977 9851214726</p>
            </div>
        </div>
        <div class="support">
            <i class="fas fa-envelope-open-text icon-color"></i>
            <div class="support-details">
                <p>Email</p>
                <p>msntraveltours@gmail.com</p>
            </div>
        </div>
        <div class="support">
            <a href="https://www.tripadvisor.com/Attraction_Review-g293890-d19406574-Reviews-Messenger_Tours_and_Travel-Kathmandu_Kathmandu_Valley_Bagmati_Zone_Central_Regio.html"
               target="_blank" class="d-flex align-items-center">
                <img class="trip" src="/img/tripadvisor.png" alt="">
                <div class="support-details">
                    <p>Recommended by</p>
                    <p>TripAdviser</p>
                </div>
            </a>
        </div>
        <div class="support">
            <img class="visitnpl" src="/img/visitnpl.png" alt="">
        </div>
    </div>
    <nav class="navbar navbar-expand-lg">
        <div class="logo">
            <a href="{{route('home')}}"> <img src="{{ asset('img/logo.png') }}" alt="no image"></a>
        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
        </button>

        <div class="collapse navbar-collapse nav_bar" id="navbarSupportedContent">
            <ul class="navbar-nav col-md-12 nav_content">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Nepal
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('destination')}}"><strong>View Popular Destinations in
                                Nepal</strong></a>
                        @foreach($nav_destinations as $destination)
                            <a class="dropdown-item font-12"
                               href="{{Route('destination.package',$destination->slug)}}">{{$destination->name}}</a>
                        @endforeach
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Tibet
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach($tibet_packages as $package)
                            <a class="dropdown-item font-12"
                               href="{{Route('package.show',$package->slug)}}">{{$package->name}}</a>
                        @endforeach
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Bhutan
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach($bhutan_packages as $package)
                            <a class="dropdown-item font-12"
                               href="{{Route('package.show',$package->slug)}}">{{$package->name}}</a>
                        @endforeach
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Adventure
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('adventure')}}"><strong>View all Adventures</strong></a>
                        @foreach($nav_adventures as $adventure)
                            <a class="dropdown-item font-12"
                               href="{{Route('adventure.show',$adventure->slug)}}">{{$adventure->name}}</a>
                        @endforeach
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Packages
                    </a>
                    <div class="dropdown-menu package-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('package')}}"><strong>View all Travel & Tour
                                Packages</strong></a>
                        <ul>
                            @foreach($nav_packages as $package)
                                <li class="@if(($loop->iteration) % 2 !== 0) float-left @else float-right @endif">
                                    <a class="dropdown-item font-12"
                                       href="{{Route('package.show',$package->slug)}}">{{$package->name}}</a>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Travel Guide
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('nepal')}}">About Nepal</a>
                        <a class="dropdown-item" href="{{route('nepal')}}">Nepal Visa Information</a>
                        <a class="dropdown-item" href="{{route('tibet')}}">About Tibet</a>
                        <a class="dropdown-item" href="{{route('tibet')}}">Tibet Travel Visa Information</a>
                        <a class="dropdown-item" href="{{route('bhutan')}}">About Bhutan</a>
                        <a class="dropdown-item" href="{{route('bhutan')}}">Tibet Bhutan Visa Information</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('blog')}}">Blogs</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('about')}}">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('contact')}}">Contact</a>
                </li>
                @if(Auth::check())
                    <li class="nav-item dropdown mt-n2">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="profile"
                                 src="@if(Auth::user()->image ){{ asset('/img/webuser/'. Auth::user()->image) }} @else {{ asset('img/profile.jpg')}} @endif"
                                 alt="user">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="right: 0; left:auto">
                            <a class="dropdown-item"
                               href="{{ route('profile') }}">My Profile</a>
                            <a class="dropdown-item" href="{{ route('profile.blog') }}">Create Blog</a>
                            <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
</header>

