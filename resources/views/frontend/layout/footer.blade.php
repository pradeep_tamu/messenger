<!-- Footer -->
<footer class="page-footer font-small mdb-color pt-4">

    <!-- Footer Links -->
    <div class="container text-center text-md-left">

        <!-- Footer links -->
        <div class="row text-center text-md-left mt-3 m-0 pb-3">

            <!-- Grid column -->
            <div class="col-md-3 col-lg-3 col-xl-3 mt-3 pl-0">
                <h5 class="text-uppercase mb-4 font-weight-bold">Messanger Tour and Travels</h5>
                <p class="text-justify">Messanger Tours and Travel was founded by a team of professional trekking and climbing
                    guides with a decade long experience in adventure tourism in the Himalaya.</p>
            </div>
            <!-- Grid column -->

            <hr class="w-100 clearfix d-md-none">

            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                <h5 class="text-uppercase mb-4 font-weight-bold">Services</h5>
                <p>
                    <a href="{{route('destination')}}">Destinations</a>
                </p>
                <p>
                    <a href="{{route('destination')}}">Tours</a>
                </p>
                <p>
                    <a href="{{route('adventure')}}">Adventures</a>
                </p>
                <p>
                    <a href="{{route('package')}}">Packages</a>
                </p>
            </div>
            <!-- Grid column -->

            <hr class="w-100 clearfix d-md-none">

            <!-- Grid column -->
            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                <h5 class="text-uppercase mb-4 font-weight-bold">Useful links</h5>
                <p>
                    <a href="{{route('login')}}">Login</a>
                </p>
                <p>
                    <a href="{{route('register')}}">Become a Member</a>
                </p>
                <p>
                    <a href="{{route('privacy')}}">Privacy Policy</a>
                </p>
                <p>
                    <a href="{{route('contact')}}">Enquiry</a>
                </p>
            </div>

            <!-- Grid column -->
            <hr class="w-100 clearfix d-md-none">

            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3 pr-0">
                <h5 class="text-uppercase mb-4 font-weight-bold">Contact</h5>
                <p>
                    <i class="fas fa-home mr-3"></i> Chaksibasri Marg, Thamel, Kathmandu</p>
                <p>
                    <i class="fas fa-envelope mr-3"></i> msntraveltours@gmail.com</p>
                <p>
                    <i class="fas fa-phone-alt mr-3"></i> +977 01 4412899</p>
                <p>
                    <i class="fas fa-phone-alt mr-3"></i> +977 9851214726</p>
            </div>
            <!-- Grid column -->

        </div>
        <!-- Footer links -->
        <h5>Certificates and Partners</h5>
        <div class="partners row">
            <div class="col-4 col-md-1">
                <img src="{{ asset('/img/visitnpl.png')}}"
                     alt="nepal logo">
            </div>
            <div class="col-4 col-md-1">
                <img src="{{ asset('/img/npl.png')}}"
                     alt="nepal logo">
            </div>
            <div class="col-4 col-md-1">
                <img src="{{ asset('/img/nma.png')}}"
                     alt="nepal logo">
            </div>
            <div class="col-4 col-md-1">
                <img src="{{ asset('/img/natta.png')}}"
                     alt="nepal logo">
            </div>
            <div class="col-4 col-md-1">
                <img src="{{ asset('/img/ntb.png')}}"
                     alt="nepal logo">
            </div>
            <div class="col-4 col-md-1">
                <img src="{{ asset('/img/taan.png')}}"
                     alt="nepal logo">
            </div>
            <div class="col-4 col-md-1">
                <img src="{{ asset('/img/tripadvisor.png')}}"
                     alt="nepal logo">
            </div>
        </div>

        <hr>
        <!-- Grid row -->
        <div class="row d-flex">

            <!-- Grid column -->
            <div class="col-md-7 col-lg-8">

                <!--Copyright-->
                <p class="text-center text-md-left">© 2019 Copyright:
                    All Rights Reserved By<strong> Messanger Tours & Travel Pvt.Ltd. </strong>Website Powered By
                    <strong><a href="https://yashrisoft.com/" target="_blank"><strong>Yashri Soft Pvt.Ltd</strong></a> </strong>
                </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-5 col-lg-4 ml-lg-0">

                <!-- Social buttons -->
                <div class="text-center text-md-right">
                    <ul class="list-unstyled list-inline">
                        <li class="list-inline-item">
                            <a href="https://www.facebook.com/msntraveltours" target="_blank"
                               class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.tripadvisor.com/Attraction_Review-g293890-d19406574-Reviews-Messenger_Tours_and_Travel-Kathmandu_Kathmandu_Valley_Bagmati_Zone_Central_Regio.html"
                               target="_blank"
                               class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fab fa-tripadvisor"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://twitter.com/MessengerAnd" target="_blank"
                               class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.youtube.com/channel/UCXvLSF61WhoabocVC5pW-jg/featured" target="_blank"
                               class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.linkedin.com/in/messanger-tours-and-travel-90615118a/" target="_blank"
                               class="btn-floating btn-sm rgba-white-slight mx-1">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

</footer>
<!-- Footer -->
