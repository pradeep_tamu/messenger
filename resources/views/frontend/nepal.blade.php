@extends('frontend.layout.layout')

@section('title', 'Tours | Travel | Visit Nepal | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

    <section class="privacy mt-5 mb-5">
        <div class="privacy-wrapper container">
            <h3><strong>Why Nepal?</strong></h3>
            <p>Nepal is the most beautiful and stunning Himalayan country in the world. Though small in size, it is
                known in
                the world as a nation of colour and contrasts-a hidden gem of nature, culture and adventure. In the
                countryside
                the way of life is still traditional, nature is at its best, high mountains and lush valleys are ideal
                places
                for trekking and mountaineering, flora and fauna invites a nature lover for anengagement with them.
                Nepal is
                rich with traditions of art and culture and Kathmandu, the capital city, is a treasure house of ancient
                art
                andculture.Nepal is a mountainous and landlocked country lying on the southern slopes of the Himalayas
                between
                India and China. Its area is 147,181 sq.km. It is 885 km east to west and 145 to 241 km north to south.
                The
                country is divided into three geographical regions.</p>

            <p>Nepal has a multiple population stemming from various cultural and ethnic communities. It has been a
                melting
                pot
                of various linguistic and ethnic groups with population 25 million living in different regions, wearing
                different costumes and speaking different languages and dialects the people belong to multi-ethnic
                groups,
                ranging from the Indo-Aryan to Mongoloid. The Himalayan and central hilly regions are mostly inhabited
                by
                the
                people of the Tibeto-Burman stock while the people inhabiting the Terai lowlands may be grouped under
                the
                Indo-Aryan category.Hinduism and Buddhism constitute two major religions in Nepal. A remarkable feature
                of
                Nepal
                is the religious homogeneity what exists, particularly between the Hindu and Buddhist communities. Apart
                from
                the Hindus and Buddhists, Muslim forms the third largest religious group.</p>

            <p> Nepal has a long glorious history. Its civilization can be traced back to thousands of years before the
                birth of
                Christ. For centuries, the kingdom of Nepal was divided into many principalities (Chaubise States).
                Kirantas
                ruled in the east, the Newars in the Kathmandu Valley, while Gurungs and Magars had their domain the
                mid-west.
                The Kirantas were said to have ruled their territories from 300 B.C. The country took its present shape
                only
                after 1768 A.D. when Prithvi Narayan Shah, king of Gorkha, conquered and united all the tiny states into
                one
                kingdom.</p>

            <p>There is perhaps no country in the world except Nepal where traditional architecture, painting and
                sculpture
                have been well preserved for 1500 years. The exquisite medieval art and architecture of the Kathmandu
                valley
                vividly reflects the artistic ingenuity and the religious tradition of the people. To better understand
                the
                deep
                and complex roots of Nepalese culture, it is necessary to visit the monuments and religious shrines.
                Nepal's climate varies with its topography. It ranges from tropical to alpine depending upon the
                altitude.
                The
                Terai region which lies in the tropical southern part of the country, for instance, has a hot & humid
                climate.
                The mid-land regions are pleasant almost all year around although winter nights are cool. The northern
                mountain
                region, around an altitude above 3353 m has an alpine climate with a considerably lower temperature in
                winter as
                can be expected.</p>

            <h3><strong>VISA Info</strong></h3>
            <p>If you are wondering about how to obtain Nepal visa, then the easiest way to obtain it is at the
                Tribhuwan
                International Airport upon your arrival in Nepal or else at the immigration entry point if you are
                entering
                through the border of India or Tibet.</p>
            <h5><strong>What do you need to obtain Nepal visa?</strong></h5>
            <p>Immigration needed a valid passport and one passport size photo with a light background. Immigration
                Department
                has not specified the size of the passport-size photo. Visa can be obtained only through payment of cash
                in
                the
                following currency: Euro, Swiss, and Franc, Pound Sterling, US dollar, Australian dollar, Canadian
                dollar,
                Hong-Kong dollar, Singapore dollar and Japanese Yen.</p>

            <h5 class="mb-3"><strong>Entry points to Nepal</strong></h5>
            <ul>
                <li>• Tribhuwan International Airport, Kathmandu</li>
                <li> • Eastern Nepal – Kakarvitta, Jhapa</li>
                <li>• Central Nepal – Birjung, Parsa</li>
                <li>• Northern Border – Kodari, Sindhupalchowk</li>
                <li>• Western – Rupandehi (Belhiya, Bhairahawa)</li>
                <li>• Mid-western – Banke (Jamuna, Nepalgunj)</li>
                <li>• Far western – Kailali (Mohana, Dhangadhi) / Kanchanpur (Gaddachauki, Mahendranagar)</li>
            </ul>

            <h5 class="mb-3 mt-3"><strong>Visa Fees</strong></h5>
            <ul>
                <li>• USD 25 or equivalent convertible currency for 15 days.</li>
                <li>• USD 40 or equivalent convertible currency for 30 days.</li>
                <li>• USD 100 or equivalent convertible currency for 90 days.</li>
                <li> • For the tourist of SAARC nationals, free visa up to 30 days.</li>
                <li>• Visa is not required for Indian nationals. However, if they enter Nepal via air, they will have to
                    show a
                    valid identification certificate (passport/ citizenship certificate). The children under the age of
                    10
                    do not
                    need to show identification certificate.
                </li>
            </ul>

            <h5 class="mb-3 mt-3"><strong>Tourist Visa Extension</strong></h5>
            <span>• Visa extension fee for 15 days or less is US $ 30 or equivalent convertible currency and visa extension fee
        for more than 15 days is US$ 3 per day</span><br>
            <span>• Tourist visa can be extended for a maximum period of 150 days in a single visa year (January – December).</span>

            <h5 class="mb-3 mt-3"><strong>Transit Visa</strong></h5>
            <p>When a foreigner has to get down at Nepalese airport as a result of his/her transit point or due to any
                force
                landing for whatever the reason, the foreigner can obtain transit visa for a day after paying USD 5 or
                any
                other
                currency equivalent.</p>
            <h5 class="mb-3"><strong>Other General Information</strong></h5>
            <span>• Entering in to Nepal or staying in Nepal without a valid visa is illegal.</span><br>
            <span>• Tourist visa can be obtained either through the Nepalese embassy or consulate or other mission offices in your
        nation or at the immigration entry points in Nepal listed above.</span><br>
            <span>• Visa once issued cannot be amended and the visa fees are non-refundable.</span><br>
            <span>• You should pay the Nepal visa fees in cash. Credit card or cheques are not acceptable.</span><br>
            <span>• Visa charges are not applicable for children under 10 years of age; however, they must obtain a valid visa.</span><br>
            <span>• If you are obtaining a new passport or travel document, you are advised to contact the Immigration Department
        for the document transfer.</span><br>
            <span>• Always carry your passport and trekking permit while on trekking.</span><br>
            <span>• Some of the areas are restricted for filming. Therefore, be careful not to do the activities that are strictly
        prohibited.</span><br>
            <span>• The culture, tradition and sentiments of Nepalese people should be respected. Do not perform any activities
        that would hurt their sentiments.</span><br>
            <span>• Do not perform any activities that would lead to moral turpitude.</span>

        </div>
    </section>

@endsection
