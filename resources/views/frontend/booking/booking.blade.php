@extends('frontend.layout.layout')

@section('title', 'Book Your Holiday with Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords','tours,travel,travel agency','bookings')

@section('content')

    <section class="booking-content container my-5">
        <h1 class="text-center text-uppercase text-bold">Book Your Holiday</h1>
        <form class="px-2 px-md-5" action="{{Route('booking.store')}}" method="POST">
            @csrf
            @if($type == 'package')
                <input type="hidden" name="package_id" value="{{$package->id}}">
            @else
                <input type="hidden" name="adventure_id" value="{{$adventure->id}}">
            @endif
            <hr>
            <div class="booking-step mt-2">
                Step 1: <span class="icon-color">Select a Trip</span>
            </div>
            <hr>
            <div class="form-group row">
                <label class="col-form-label col-3 col-md-1" for="trip">Trip:</label>
                <div class="col-9 col-md-11">
                    <input type="text"
                           class="form-control-plaintext"
                           name="trip"
                           id="trip"
                           value="@if($type == 'package'){{$package->name}}@else{{$adventure->name}}@endif"
                           disabled>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12 col-md-6">
                    <label class="col-form-label" for="trip_start_date">Start Date:</label>
                    <input type="date"
                           class="form-control  @if($errors->first('trip_start_date')) is-invalid @endif"
                           name="trip_start_date"
                           id="trip_start_date"
                           value="{{old('trip_start_date')}}"
                           placeholder="Trip Start Date"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('trip_start_date') }}</span>
                </div>
                <div class="col-12 col-md-6">
                    <label class="col-form-label" for="number_of_people">Number of People:</label>
                    <input type="number"
                           class="form-control  @if($errors->first('number_of_people')) is-invalid @endif"
                           name="number_of_people"
                           id="number_of_people"
                           placeholder="Number of People"
                           value="{{old('number_of_people')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('number_of_people') }}</span>
                </div>
            </div>

            <hr>
            <div class="booking-step mt-2">
                Step 2: <span class="icon-color">Personal Information</span>
            </div>
            <hr>
            <div class="form-group row">
                <div class="col-12 col-md-6">
                    <label class="col-form-label" for="first_name">First Name:</label>
                    <input type="text"
                           class="form-control  @if($errors->first('first_name')) is-invalid @endif"
                           name="first_name"
                           id="first_name"
                           placeholder="First Name"
                           value="{{old('first_name')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('first_name') }}</span>
                </div>
                <div class="col-12 col-md-6">
                    <label class="col-form-label" for="last_name">Last Name:</label>
                    <input type="text"
                           class="form-control  @if($errors->first('last_name')) is-invalid @endif"
                           name="last_name"
                           id="last_name"
                           placeholder="Last Name"
                           value="{{old('last_name')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('last_name') }}</span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12 col-md-6">
                    <label class="col-form-label" for="dob">Date of Birth:</label>
                    <input type="date"
                           class="form-control  @if($errors->first('dob')) is-invalid @endif"
                           name="dob"
                           id="dob"
                           placeholder="Date of Birth"
                           value="{{old('dob')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('dob') }}</span>
                </div>
                <div class="col-12 col-md-6">
                    <label class="col-form-label" for="country">Country:</label>
                    <input type="text"
                           class="form-control  @if($errors->first('country')) is-invalid @endif"
                           name="country"
                           id="country"
                           placeholder="Country"
                           value="{{old('country')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('country') }}</span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12 col-md-6">
                    <label class="col-form-label" for="email">Phone Number:</label>
                    <input type="text"
                           class="form-control  @if($errors->first('phone')) is-invalid @endif"
                           name="phone"
                           id="phone"
                           placeholder="Phone Number"
                           value="{{old('phone')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('phone') }}</span>
                </div>
                <div class="col-12 col-md-6">
                    <label class="col-form-label" for="email">Email Address:</label>
                    <input type="email"
                           class="form-control  @if($errors->first('email')) is-invalid @endif"
                           name="email"
                           id="email"
                           placeholder="Email Address"
                           value="{{old('email')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('email') }}</span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12 col-md-6">
                    <label class="col-form-label" for="passport_number">Passport Number:</label>
                    <input type="text"
                           class="form-control  @if($errors->first('passport_number')) is-invalid @endif"
                           name="passport_number"
                           id="passport_number"
                           placeholder="Passport Number"
                           value="{{old('passport_number')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('passport_number') }}</span>
                </div>
                <div class="col-12 col-md-6">
                    <label class="col-form-label" for="passport_expiration_date">Passport Expiration Date:</label>
                    <input type="date"
                           class="form-control  @if($errors->first('passport_expiration_date')) is-invalid @endif"
                           name="passport_expiration_date"
                           id="passport_expiration_date"
                           placeholder="Passport Expiration Date"
                           value="{{old('passport_expiration_date')}}"
                           required>
                    <span class="invalid-feedback d-block"
                          role="alert">{{ $errors->first('passport_expiration_date') }}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-form-label" for="mailing_address">Mailing Address:</label>
                <textarea type="text"
                          class="form-control  @if($errors->first('mailing_address')) is-invalid @endif"
                          name="mailing_address"
                          id="mailing_address"
                          placeholder="Mailing Address"
                          required>{{old('mailing_address')}}</textarea>
                <span class="invalid-feedback d-block" role="alert">{{ $errors->first('mailing_address') }}</span>
            </div>
            <div class="form-group row">
                <div class="col-12 col-md-6 col-lg-4">
                    <label class="col-form-label" for="emergency_contact_name">Emergency Contact Name:</label>
                    <input type="text"
                           class="form-control  @if($errors->first('emergency_contact_name')) is-invalid @endif"
                           name="emergency_contact_name"
                           id="emergency_contact_name"
                           placeholder="Emergency Contact Name"
                           value="{{old('emergency_contact_name')}}"
                           required>
                    <span class="invalid-feedback d-block"
                          role="alert">{{ $errors->first('emergency_contact_name') }}</span>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <label class="col-form-label" for="emergency_contact_relation">Relationship:</label>
                    <input type="text"
                           class="form-control  @if($errors->first('emergency_contact_relation')) is-invalid @endif"
                           name="emergency_contact_relation"
                           id="emergency_contact_relation"
                           placeholder="Relationship"
                           value="{{old('emergency_contact_relation')}}"
                           required>
                    <span class="invalid-feedback d-block"
                          role="alert">{{ $errors->first('emergency_contact_relation') }}</span>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <label class="col-form-label" for="emergency_contact_phone">Contact:</label>
                    <input type="text"
                           class="form-control  @if($errors->first('emergency_contact_phone')) is-invalid @endif"
                           name="emergency_contact_phone"
                           id="emergency_contact_phone"
                           placeholder="Emergency Contact"
                           value="{{old('emergency_contact_phone')}}"
                           required>
                    <span class="invalid-feedback d-block"
                          role="alert">{{ $errors->first('emergency_contact_phone') }}</span>
                </div>
            </div>

            <hr>
            <div class="booking-step mt-2">
                Step 3: <span class="icon-color">Flight Details</span>
            </div>
            <hr>
            <div class="form-group row">
                <div class="col-12 col-md-6 col-lg-3">
                    <label class="col-form-label" for="arrival_date">Arrival Date:</label>
                    <input type="date"
                           class="form-control  @if($errors->first('arrival_date')) is-invalid @endif"
                           name="arrival_date"
                           id="arrival_date"
                           placeholder="Arrival Date"
                           value="{{old('arrival_date')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('arrival_date') }}</span>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <label class="col-form-label" for="arrival_time">Arrival Time:</label>
                    <input type="time"
                           class="form-control  @if($errors->first('arrival_time')) is-invalid @endif"
                           name="arrival_time"
                           id="arrival_time"
                           placeholder="Arrival Time"
                           value="{{old('arrival_time')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('arrival_time') }}</span>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <label class="col-form-label" for="arrival_flight_number">Arrival Flight Number:</label>
                    <input type="text"
                           class="form-control  @if($errors->first('arrival_flight_number')) is-invalid @endif"
                           name="arrival_flight_number"
                           id="arrival_flight_number"
                           placeholder="Arrival Flight Number"
                           value="{{old('arrival_flight_number')}}"
                           required>
                    <span class="invalid-feedback d-block"
                          role="alert">{{ $errors->first('arrival_flight_number') }}</span>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <label class="col-form-label" for="pickup">Pickup at Airport:</label>
                    <select class="form-control  @if($errors->first('email')) is-invalid @endif"
                            name="pickup"
                            id="pickup"
                            required>
                        <option value="">Select One</option>
                        <option value="Required" @if (old('pickup') == "Required") {{ 'selected' }} @endif>Required</option>
                        <option value="Not Required" @if (old('pickup') == "Not Required") {{ 'selected' }} @endif>Not Required</option>
                    </select>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('email') }}</span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12 col-md-6 col-lg-3">
                    <label class="col-form-label" for="departure_date">Departure Date:</label>
                    <input type="date"
                           class="form-control  @if($errors->first('departure_date')) is-invalid @endif"
                           name="departure_date"
                           id="departure_date"
                           placeholder="Departure Date"
                           value="{{old('departure_date')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('departure_date') }}</span>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <label class="col-form-label" for="departure_time">Departure Time:</label>
                    <input type="time"
                           class="form-control  @if($errors->first('departure_time')) is-invalid @endif"
                           name="departure_time"
                           id="departure_time"
                           placeholder="Departure Time"
                           value="{{old('departure_time')}}"
                           required>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('departure_time') }}</span>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <label class="col-form-label" for="departure_flight_number">Departure Flight Number:</label>
                    <input type="text"
                           class="form-control  @if($errors->first('departure_flight_number')) is-invalid @endif"
                           name="departure_flight_number"
                           id="departure_flight_number"
                           placeholder="Departure Flight Number"
                           value="{{old('departure_flight_number')}}"
                           required>
                    <span class="invalid-feedback d-block"
                          role="alert">{{ $errors->first('departure_flight_number') }}</span>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <label class="col-form-label" for="pickup">Drop off at Airport:</label>
                    <select class="form-control  @if($errors->first('email')) is-invalid @endif"
                            name="drop_off"
                            id="drop_off"
                            required>
                        <option value="">Select One</option>
                        <option value="Required" @if (old('drop_off') == "Required") {{ 'selected' }} @endif>Required</option>
                        <option value="Not Required" @if (old('drop_off') == "Not Required") {{ 'selected' }} @endif>Not Required</option>
                    </select>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('email') }}</span>
                </div>
            </div>

            <hr>
            <div class="booking-step mt-2">
                Step 4: <span class="icon-color">Travel Insurance</span>
            </div>
            <hr>
            <div class="form-group">
                <p>
                    Travel insurance is mandatory for any trek or adventure which must cover all the
                    medical and emergency evaluation.
                </p>
                <p>
                    The tours below 2500 meters of altitude, insurance is not mandatory.
                </p>
                <label for="insurance">Please select one:</label><br>
                <input type="radio"
                       class="custom-radio ml-2"
                       name="insurance"
                       value="I already have insurance" @if (old('insurance') == "I already have insurance") checked @endif> I already have insurance.<br>
                <input type="radio"
                       class="custom-radio ml-2"
                       name="insurance"
                       value="I will buy one later" @if (old('insurance') == "I will buy one later") checked @endif> I will buy one later.<br>
                <input type="radio"
                       class="custom-radio ml-2"
                       name="insurance"
                       value="My trip does not require one" @if (old('insurance') == "My trip does not require one") checked @endif> My trip does not require one.<br>
                <span class="invalid-feedback d-block" role="alert">{{ $errors->first('insurance') }}</span>
                <p class="mt-1">
                    <strong>Note: </strong> If your trip requires insurance, please provide a copy of your insurance
                    upon your arrival in
                    Kathmandu. In case you fail to do so, you will have to buy one.
                </p>
            </div>

            <hr>
            <div class="booking-step mt-2">
                <span class="icon-color">Special Requirement </span>
            </div>
            <hr>
            <div class="form-group row">
                <div class="col-12">
                    <label class="col-form-label" for="message">
                        Message: (Optional) <small>If you have any special requirement then you can leave us a
                            message.</small>
                    </label>
                    <textarea type="text"
                              class="form-control  @if($errors->first('message')) is-invalid @endif"
                              name="message"
                              id="message"
                              placeholder="Message"
                              required>{{old('message')}}</textarea>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('message') }}</span>
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <div class="col-12 col-md-6 col-lg-4">
                    <label class="col-form-label" for="reference">How did you hear about us? </label>
                    <select class="form-control  @if($errors->first('email')) is-invalid @endif"
                            name="reference"
                            id="reference"
                            required>
                        <option value="">Select one...</option>
                        <option
                            value="Family/Friends" @if (old('reference') == "Family/Friends") {{ 'selected' }} @endif>
                            Family/Friends
                        </option>
                        <option value="Advertisement" @if (old('reference') == "Advertisement") {{ 'selected' }} @endif>
                            Advertisement
                        </option>
                        <option value="Website" @if (old('reference') == "Website") {{ 'selected' }} @endif>Website
                        </option>
                        <option value="Blog" @if (old('reference') == "Blog") {{ 'selected' }} @endif>Blog</option>
                        <option
                            value="Referred by someone" @if (old('reference') == "Referred by someone") {{ 'selected' }} @endif>
                            Referred by someone
                        </option>
                    </select>
                    <span class="invalid-feedback d-block" role="alert">{{ $errors->first('email') }}</span>
                </div>
            </div>

            <hr>
            <div class="form-group">
                <input type="checkbox"
                       class="custom-checkbox"
                       name="agree" required> I agree to Terms and Conditions.
            </div>
            <hr>
            <div class="form-group">
                <button type="submit" class="btn icon-bg col-5 col-md-2 text-white mr-3">BOOK</button>
                <button type="reset" class="btn btn-secondary col-5 col-md-2">RESET</button>
            </div>
            <hr>
        </form>
    </section>

@endsection
