@extends('frontend.landing.layout')

@section('content')

    <div class="container mb-5 mt-5">
        <div class="card">
            @if(session()->has('success'))
                <div class="alert alert-success" style="margin-bottom: 0;text-align: center;">
                    {{ session()->get('success') }}
                </div>
            @endif
            <script>
                $(document).ready(function () {
                    setTimeout(function () {
                        $('.alert').hide('slow');
                    }, 3000)
                });
            </script>

        </div>

        <div class="payment-details privacy-wrapper mt-5">
            <p>Messanger travel and tours is a reliable government registered trekking company in Nepal committed to
                offering the best possible services to all of you. With a purpose of making tourism better, we put our
                efforts into making our service better and better. Your pleasure and joy is our priority! Thank you for
                choose us; your booking has been forwarded to our Travel Consultant. Please proceed for booking deposit
                payment. We will confirm your payments as soon as we receive.</p>

            <h5><strong>Booking Deposit Payment</strong></h5>
            <h6><strong>(a) Nepal Trips:</strong></h6>
            <p>
                For Booking the Trips, Provide us a photocopy of your passport and 25% of trip cost as a deposit while
                booking your trip with us. The deposit amount is nonrefundable. You can pay the remaining amount ahead
                your arrival in Kathmandu and before the trip departure.
            </p>
            <p>
                The non-refundable deposit should be sent to the Company or its Agent. The Company’s Tailor Made trips
                may require a higher deposit or full payment at the time of booking. If deposit is different from that
                outlined in this clause, you will be advised by the company at the time of booking.
            </p>

            <h6><strong>(b) India/Tibet/ Bhutan Trips:</strong></h6>
            <p>In the case of Bhutan and Tibet tours, At time of booking a non-refundable deposit of 25% is due along
                with due airfares (if client wants the company to arrange it) before 25 days of trip departure. If
                you're booking 25 days or less prior to departure, full payment is due. We reserve our right to cancel
                your trip if you could not make your payment within our stipulated time.</p>

            <h5><i><strong>Payment Methods:</strong></i></h5>
            <p>You can make your payment following either of the methods:</p>

            <h6><strong>(1) Payment by Credit Card (MasterCard or Visa Only)</strong></h6>
            <p>
                Please complete the necessary details in the form including your credit card details, signature (same as
                in your passport & Credit Card. Your credit card will be charged by Himalayan Bank (Credit Card
                Division) on behalf of Messanger travel and tours.
            </p>
            <p><strong>Note:</strong> Please don’t send the form details directly to Himalayan Bank. Please also send copy of your
                passport or any ID copy along with the filled form for secure payment. And there is 3.52% surcharge when
                you make the payment with a credit card.</p>

            <h5><strong>(2)SWIFT/ Wire Transfer:</strong></h5>

            <strong>Pay to:</strong> Messanger tours travel PVT. Ltd <br>
            <strong>Account Number:</strong> 0570010154246<br>
            <strong>Paying Bank:</strong> Mega Bank Nepal Ltd<br>
            <strong>Branch:</strong> Thamel<br>
            <strong>Street address:</strong> Thamel BhagwatiMarg, Kathmandu, Nepal<br>
            <strong>Swift Code:</strong> MBNLNPKA

        </div>
    </div>

@stop
