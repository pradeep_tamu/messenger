@extends('frontend.layout.layout')

@section('title', 'Tours | Travel | Visit Nepal | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

    <section class="landing-section">
        <div class="landing-slides owl-carousel" id="owl-carousel">
        @foreach($slides as $slide)
            <!-- Single Welcome Slide -->
                <div class="single-landing-slide bg-overlay slider-landing"
                     data-img-url="{{ asset('/img/slider/'.$slide->filename) }}">
                    <img src="{{ asset('/img/slider/'.$slide->filename) }}"
                         alt="" style="opacity: 0.6">
                    <div class="landing-text">
                        <div class="title">
                            Explore The Hidden Nepal
                        </div>
                        @if(!Auth::check())
                            <div class="login-area">
                                <a href="{{route('register')}}">
                                    <button class="icon-bg">Sign Up</button>
                                </a>
                                <a href="{{route('login')}}">
                                    <button class="icon-bg">Login</button>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </section>

    <div class="search-form-area">
        <div class="container-fluid">
            <div class="search-form">
                <form action="#" method="post">
                    <div class="row justify-content-between align-items-end">
                        <div class="col-md-12 col-lg-3">
                            <select class="form-control" id="destination" name="destination">
                                <option value="0">Select Destination</option>
                                @foreach($nav_destinations as $destination)
                                    <option value="{{ $destination->id }}">{{ $destination->name }}</option>
                                @endforeach
                                {{ csrf_field() }}
                            </select>
                        </div>
                        <div class="col-md-12 col-lg-3">
                            <select class="form-control" id="package" name="package" disabled>
                                <option value="0">Check Available Packages</option>
                            </select>
                        </div>
                        <div class=" col-md-12 col-lg-3">
                            <input type="text" class="form-control" id="duration" name="duration"
                                   placeholder="Duration" disabled>
                        </div>
                        <div class="col-md-12 col-lg-3" id="findbutton">
                            <button class="form-control btn landing-btn w-100" disabled
                                    style="font-weight: bolder; color: black;">
                                Find Your Tour
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container mt-50 ipad" style="max-width: 80% !important;">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6">
                <div class="section-heading">
                    <h2>Popular Destinations</h2><br>
                    <a href="{{route('destination')}}"><h6>View all destinations</h6></a>
                </div>
                <div class="about-us-content mb-50">
                    <h6>Hiking up from the snow-covered tundra paradise resembles nothing that you have ever experienced
                        everywhere else on earth and trekking in Nepal is a completely magnificent ordeal. Nepal has
                        numerous different destinations for trekking around the country.
                    </h6>
                </div>
            </div>

            <div class="col-12 col-lg-6">
                <div class="about-us-thumbnail mb-70">
                    <div class="row no-gutters">
                        @foreach($destination_image as $destination)
                            <div class="col-6">
                                <div class="single-thumb">
                                    <a href="{{route('destination')}}">
                                        <img src="{{ asset('/img/thumbnail/destination/'.$destination->filename) }}"
                                             alt="destinations image">
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="packages-area d-flex justify-content-center align-items-center popular">
        <h5>Popular Packages / </h5>
        <a href="{{route('package')}}"><h6 class="icon-color">&nbsp; View All Packages</h6></a>
    </div>

    <div class="package_display container owl-carousel">
        @foreach($packages as $package)
            <div class="img-section  item">
                <div class="slide-img">
                    <a href="{{route('package.show', $package->slug)}}">
                        <img src="{{ asset('/img/thumbnail/package/'.$package->filename) }}"
                             alt="nepal tour package image">
                    </a>
                </div>
                <div class="pacakage-desc">
                    <div class="title-desc">
                        <div class="desc-title">{{$package->name}}</div>
                        <div class="price">
                            <div class="discount">
                                From
                            </div>
                            <div class="actual-price">
                                ${{$package->cost}}
                            </div>
                        </div>
                    </div>
                    <div class="review">
                        <h6>
                            @for($i=1;$i<=$package->averageRating();$i++)
                                <i class="fas fa-star active_star"></i>
                            @endfor
                            @for($i=$package->averageRating();$i<5;$i++)
                                <i class="fas fa-star"></i>
                            @endfor
                            {{ $package->ratingPercent(5) }}%
                        </h6>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <section class="blog-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="packages-area d-flex justify-content-center align-items-center popular">
                        <h5>Blog Section / </h5>
                        <a href="{{route('blog')}}"><h6 class="icon-color">&nbsp; View All Blogs</h6></a>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach($blogs as $blog)
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="single-blog mb-100">
                            <div class="blog-img">
                                <a href="#" class="blog-post-image"> <img
                                        src="{{ asset('/img/thumbnail/blog/'.$blog->image) }}"
                                        alt="travel nepal blog image"></a>
                            </div>
                            <div class="blog-parts">
                                <div class="post-meta">
                                    <a href="#"
                                       class="post-date"> {{ \Carbon\Carbon::parse($blog->created_at)->diffForHumans() }}</a>
                                </div>
                                <a href="#" class="post-title"> {{ substr($blog->title,0,50) }}</a>
                                <p style="color: rgb(120,120,120)">{!! substr($blog->body,0,90) !!}</p>
                                <a href="{{ route('blog.show', $blog->slug) }}">
                                    <button class="icon-bg">Read More</button>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <div class="book-us">
        <div class="book-us-content-list col-12 col-lg-3" style="background: #4263c1;">Why Book With Us ?</div>
        <div class="book-us-content-list col-12 col-lg-3" style="background: #4787e8;">
            <i class="fas fa-hiking"></i>
            <div class="desc">
                <h5>100+ Destinations</h5>
                <h6>Explore Nepal With Us</h6>
            </div>
        </div>
        <div class="book-us-content-list col-12 col-lg-3" style="background: #4787e8;">
            <i class="fas fa-money-bill-alt"></i>
            <div class="desc">
                <h5>Best Price Guarantee</h5>
                <h6>You wont Regret</h6>
            </div>
        </div>
        <div class="book-us-content-list col-12 col-lg-3" style="background: #4787e8;">
            <i class="fas fa-users-cog"></i>
            <div class="desc">
                <h5>Great Customer Service</h5>
                <h6>You'll Never Walk Alone</h6>
            </div>
        </div>
    </div>

@endsection
