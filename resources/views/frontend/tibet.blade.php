@extends('frontend.layout.layout')

@section('title', 'Tours | Travel | Visit Nepal | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

<section class="privacy mt-5 mb-5">
    <div class="privacy-wrapper container">
        <h3><strong>Discover Tibet(Roof of the World): </strong></h3>
        <p>Most importantly, all travelers traveling in Tibet are kindly requested to bear in mind that Tibet being
            extremely remote isolated by the most formidable Himalayas ranges, remains still one of the most captivating
            but least developed parts in the world. With its very short history of tourism (just about 12 years), the
            facilities for tourists although being upgraded are still at basic and limited scale. So, the visitors are
            requested not have high expectation in terms of facilities in Tibet. You can rather take this tour as an
            adventure from every point of view. However, we will always put all our efforts to make your journey as
            pleasant as possible.</p>

        <h5 class="mb-3"><strong>Six Fun Facts about Tibet</strong></h5>
        <ul>
            <li>1. The plateau of Tibet is the world’s highest plateau with an average elevation of over 4,500 meters.
            </li>
            <li>2. The yak is an integral part of Tibetan culture.</li>
            <li>3. The currency of Tibet is Chinese Yuan Renminbi.</li>
            <li>4. The staple food of Tibetan is Tsampa (roasted barley flour), while the national drink is salted
                butter tea.
            </li>
            <li>5. Tibet Autonomous Region, China is predominantly Buddhist.</li>
            <li>6. There are two world Heritage sites located in Tibet, Potala and Norbuligka palaces.</li>
        </ul>


        <h5><strong>Overcoming altitude problems</strong></h5>
        <p>Traveling in Tibet is an adventure involving high altitude and could be strenuous. So far, most visitors have
            only minor effects from the altitude. However, we advise especially the guests with known heart or lungs or
            blood diseases to consult their doctor before traveling. Mild headache, fever, loss of appetite or stomach
            disorder can take place before acclimatization. Our advice: Drink 4 ltr of water minimum a day. Do not
            exhaust yourself of much and breathe deep and take rest more than usual. Bottled water is available in each
            hotel where you have overnight and in the restaurant en route where you have your lunch.</p>

        <h5><strong>Landslides</strong></h5>
        <p>As Tibet tours get opened mainly during the monsoon time; there are high chances of landslides mostly in the
            Nepalese part (also in the early part in Tibet). In case of landslides, to over cross them and the gap in
            between, extra vehicle with porters may have to be arranged. In that case, you are kindly requested to
            contribute nominal charges for porters and vehicle, ranging from Rs. 200 to 300 per person. We wish there
            will be no hassles as such.</p>

        <h5><strong>Entering Tibet</strong></h5>
        <p><strong>Nepal</strong></p>
        <p><strong>By Air:</strong> So far, there are two direct flights between Lhasa and Kathmandu, run by Sichuan
            Airline and Air China.
            The best thing about flying to Tibet from Nepal is the stunning bird’s-eye view of the Himalayas including
            Mt.
            Everest, the world’s highest peak.</p>

        <p><strong>By Road:</strong>This is the most common way to enter Tibet. Thanks to many cheap direct
            international flights to
            Kathmandu, many tourists find it convenient to fly to Kathmandu then enter Tibet via land. However, if you
            are
            already in Nepal, you can enter Tibet via Kerung, Simikot and Kodari borders.</p>
        <p><strong>Note that the Simikot border is used mostly by travelers wishing to tour/trek Kailash. There are
                plenty of
                cars,
                vans, buses that frequent the Kerung border.</strong></p>

        <h5><strong>Passport and Visa Information</strong></h5>
        <p>Travelling to Tibet requires a Chinese visa and a Tibet Travel Permit which can both be obtained in Kathmandu
            with the help of a travel company (Tibet via Nepal). A valid visa for China is not the same as a Tibet
            Travel
            Permit. Those with a Chinese tourist visa will still need to apply for a Tibet travel permit. The permit is
            still required for foreign travelers travelling to Tibet from mainland China. To acquire the permit you need
            to
            book a guide for your entire trip and pre-arrange private transport for trips outside Lhasa. The trip
            outside
            Lhasa also requires additional permits which are arranged by the company you are travelling with.</p>

        <h5><strong>Travel Insurance</strong></h5>
        <p>Tibet is a remote location, and if you become seriously injured or very sick, you may need to be evacuated by
            air. Under these circumstances, you don’t want to be without adequate health insurance. Be sure your policy
            covers evacuation.</p>

        <h3><strong>Best Time to Visit Tibet</strong></h3>
        <p>May to September is the most popular season to visit Tibet. The weather is warm with clear skies. The
            snow/ice
            starts melting from April clearing the blocked roads and making it easier for you to visit various Tibetan
            townships. However, since this is the peak season, the prices are at their highest. If you want to save
            around 20% of your money you can visit Tibet in either April or between October to November.
            The weather is cold but there are not a lot of tourists visiting so you get more options for hotels and
            vehicles.</p>
        <p>The lowest tourist season in Tibet is winter (Dec–Feb). The weather is very cold but you have all the
            attractions to yourself. The hotels and transport are considerably cheaper which means you can get hotels
            and
            vehicles in half the price you would pay during the peak tourist season.</p>

        <h3><strong>Tibet Travel Visa</strong></h3>
        <h5><strong>Tibet/China Travel Visa</strong></h5>
        <p>If you are entering Tibet from Kathmandu, you should get your Chinese visa from the Chinese Embassy in Kathmandu
        (regardless of whether you already have a Chinese visa issued in your country). Please note that if you have
        already got a Chinese visa at your home country, it will be cancelled and fresh visa will be issued in
        Kathmandu. So do not apply for a Chinese visa until you reach to Kathmandu and avoid waste of money (in the case
            you enter Tibet from Nepal).</p>

        <p>However if you are entering Tibet form mainland China or Honking, get your Chinese visa in advance. You are
        required to hold a valid Chinese visa (issued by a Chinese Embassy and stamped in your individual passport), as
        well as a valid passport to enter Tibet (at least six months valid). When applying for a Chinese visa at your
            home country, it is better not mentioning that you intend to visit Tibet.</p>

        <p>In order to get Chinese visa in Kathmandu, please send us your clear passport copy and other details like your
        profession to us at least 15 days in advance. We will get Tibet Travel Permit (TTB permit or commonly called
        Invitation letter to travel to Tibet from the Tibet Travel Bureau - TTB). Once you arrive to Kathmandu, we apply
        for your Chinese visa along with your original passport and TTB permit. You will get a group visa (it will be
            not stamped in your individual passport but will be in a sheet of paper).</p>

       <p> Very important: The visa days in the Chinese Embassy in Kathmandu is Monday, Wednesday and Friday (and visa can
        be collected the same day upon payment of urgent fees – USD 85 for European passports and USD 175for US
        passports ). In order not to waste your time in Kathmandu, make it such that you arrive to Kathmandu either
        Sunday or Tuesday or Thursday so that you can get visa the next day and fly to Tibet the day after. (Except in
        main season, the flights to Lhasa from Kathmandu are generally operated on Tuesday, Thursday and Saturday
           mornings and almost always get packed).</p>

        <h5><strong>Tibet Travel Permits</strong></h5>
        <p><strong>There are two different permits required to travel to Tibet</strong></p>
        <p>1. The entry permit (TTB permit, equivalent of a visa for Tibet, required to enter Tibet region of China. A
        Chinese visa and Tibet entry permit are both required to enter Tibet). You need to show this to the authorities
            in airport or surface entry points.</p>
        <p>2. The Alien travel permit (PSB permit) which is required to travel restricted areas in Tibet. In practice you
        need PSB permit to travel any regions outside Lhasa. Different PSB permits are required to travel to different
        parts of Tibet. PSB permits are issued by local police authorities called Public Security Bureau (PSB).
            How to get the travel permits?</p>
        <p>You send us the following details exactly as in your passport at least 15 days before the trip date.</p>
        <ul>
            <li>1) Your full name;</li>
            <li>2) Your gender;</li>
            <li>3) Your date of birth;</li>
            <li>4) Your passport number;</li>
            <li>5) Your nationality</li>
            <li>6) Your profession</li>
            <li>Or simply send us a clear scan copy of your passport.</li>
        </ul>

        <p><strong>Please note</strong> that mentioning your true profession is important as journalists, government officials, political
        personnel and diplomats may require more complicated procedures in order to be issued a permit. We do not
        process for them nor would we be liable if travel permits are not issued at the last moment because you belong
        to this category of profession but do not inform us in time. Please note that neither we nor any travel agency
            will / can sell "permit-only" service. One must buy the tour package to obtain it.</p>


    </div>
</section>

@endsection
