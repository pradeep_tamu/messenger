@extends('frontend.layout.layout')

@section('title', 'Login | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')
    <div class="card auth-card password-request">
        <img src="img/123.jpg" class="card-img">
        <div class="col-md-4 card-img-overlay login-layout d-flex justify-content-center align-items-center">
            <div class="card-body">
                @if(Session::has('message'))
                    <div class="row justify-content-center">
                        <div class="d-block text-center mb-4" style="color: #227dc7">
                            <i class="fas fa-check-circle mr-2"></i>{{ Session::get('message') }}
                        </div>
                    </div>
                @endif

                <h4 class="card-title">Password Reset</h4>
                <hr>
                <form method="POST" action="{{ route('password.reset')}}">
                    @csrf
                    <input type="hidden" value="{{$token}}" name="token">
                    <div class="form-group">
                        <input type="text"
                               class="form-control @if($errors->first('email')) is-invalid @endif"
                               name="email"
                               value="{{ old('email') }}"
                               placeholder="Email">
                        <span class="invalid-feedback d-block" role="alert">{{ $errors->first('email') }}</span>
                    </div>
                    <div class="form-group">
                        <input type="password"
                               class="form-control @if($errors->first('password')) is-invalid @endif"
                               id="password"
                               name="password"
                               placeholder="Password"
                               required>
                        <span class="invalid-feedback d-block" role="alert">{{ $errors->first('password') }}</span>
                    </div>
                    <div class="form-group">
                        <input type="password"
                               class="form-control @if($errors->first('password_confirmation')) is-invalid @endif"
                               id="password_confirmation"
                               name="password_confirmation"
                               placeholder="Confirm Password"
                               required>
                        <span class="invalid-feedback d-block"
                              role="alert">{{ $errors->first('password_confirmation') }}</span>
                    </div>
                    <hr>
                    <div>
                        <input type="submit" class="btn btn-primary icon-bg mr-3" value="Reset">
                        <input type="reset" class="btn btn-secondary" value="Clear">
                    </div>
                    @if(Session::has('error'))
                        <div class="row justify-content-center mt-3">
                            <div class="text-center invalid-feedback d-block mb-2">
                                <i class="fas fa-exclamation-circle mr-2"></i>{{ Session::get('error') }}
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>

@endsection


