@extends('frontend.layout.layout')

@section('title', 'Login | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')
    <div class="card auth-card login">
        <img src="img/123.jpg" class="card-img">
        <div class="col-md-4 card-img-overlay login-layout text-center">
            <div class="card-body">
                @if(Session::has('message'))
                    <div class="row justify-content-center">
                        <div class="d-block text-center mb-4" style="color: #227dc7">
                            <i class="fas fa-check-circle mr-2"></i>{{ Session::get('message') }}
                        </div>
                    </div>
                @endif

                <h5 class="card-title">Login</h5>
                <hr>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <input type="text"
                               class="form-control @if($errors->first('email')) is-invalid @endif"
                               name="email"
                               value="{{ old('email') }}"
                               placeholder="Email">
                        <span class="invalid-feedback d-block" role="alert">{{ $errors->first('email') }}</span>
                    </div>
                    <div class="form-group">
                        <input type="password"
                               class="form-control @if($errors->first('password')) is-invalid @endif"
                               name="password"
                               placeholder="Password">
                        <span class="invalid-feedback d-block" role="alert">{{ $errors->first('password') }}</span>
                    </div>
                    @if(Session::has('error'))
                        <div class="row justify-content-center">
                            <div class="text-center invalid-feedback d-block mb-2">
                                <i class="fas fa-exclamation-circle mr-2"></i>{{ Session::get('error') }}
                            </div>
                        </div>
                    @endif
                    <hr>
                    <div class="row m-0 justify-content-center">
                        <input type="submit" class="btn btn-primary icon-bg mr-3" value="Login">
                        <input type="reset" class="btn btn-secondary" value="Clear">
                    </div>
                    <div class="row mx-0 mt-2 justify-content-center">
                        <a href="{{route('password.request')}}" class="icon-color">Forget Password?</a>
                    </div>
                </form>
                <hr>
                <p class="row justify-content-center"><strong class="text-muted">Don't have an account? <a
                            href="{{ route('register') }}">Register</a> here </strong></p>
                <p class="row justify-content-center text-bold" style="color: black">OR</p>
                <div class="row justify-content-center">
                    <social-login></social-login>
                </div>
            </div>
        </div>
    </div>

@endsection


