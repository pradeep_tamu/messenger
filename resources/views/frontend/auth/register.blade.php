@extends('frontend.layout.layout')

@section('title', 'Register | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to
                            Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')

    <div class="card auth-card register">
        <img src="img/123.jpg" class="card-img">
        <div class="col-md-4 card-img-overlay login-layout text-center">

            <div class="card-body">
                <h5 class="card-title">Register</h5>
                <hr>
                <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <input type="text"
                               value="{{ old('name') }}"
                               class="form-control @if($errors->first('name')) is-invalid @endif"
                               id="name"
                               name="name"
                               placeholder="Name"
                               required autofocus>
                        <span class="invalid-feedback d-block" role="alert">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group">
                        <input type="email"
                               value="{{ old('email') }}"
                               class="form-control @if($errors->first('email')) is-invalid @endif"
                               id="email"
                               name="email"
                               placeholder="Email"
                               required>
                        <span class="invalid-feedback d-block" role="alert">{{ $errors->first('email') }}</span>
                    </div>
                        <div class="form-group">
                            <input type="password"
                                   class="form-control @if($errors->first('password')) is-invalid @endif"
                                   id="password"
                                   name="password"
                                   placeholder="Password"
                                   required>
                            <span class="invalid-feedback d-block" role="alert">{{ $errors->first('password') }}</span>
                        </div>
                        <div class="form-group">
                            <input type="password"
                                   class="form-control @if($errors->first('password_confirmation')) is-invalid @endif"
                                   id="password_confirmation"
                                   name="password_confirmation"
                                   placeholder="Confirm Password"
                                   required>
                            <span class="invalid-feedback d-block"
                                  role="alert">{{ $errors->first('password_confirmation') }}</span>
                        </div>
                    <div class="form-group">
                        <input type="text"
                               value="{{ old('country') }}"
                               class="form-control @if($errors->first('country')) is-invalid @endif"
                               id="country"
                               name="country"
                               placeholder="Country"
                               required>
                        <span class="invalid-feedback d-block" role="alert">{{ $errors->first('country') }}</span>
                    </div>
                    <div class="form-group">
                        <input type="number"
                               value="{{ old('phone') }}"
                               class="form-control @if($errors->first('phone')) is-invalid @endif"
                               id="phone"
                               name="phone"
                               placeholder="Phone"
                               required>
                        <span class="invalid-feedback d-block" role="alert">{{ $errors->first('phone') }}</span>
                    </div>
                    <div class="form-group row m-0">
                        <label for="image">Profile Picture: <small class="text-danger"><i>Optional</i></small> </label>
                        <input type="file"
                               value="{{ old('image') }}"
                               class="form-control-file @if($errors->first('image')) is-invalid @endif"
                               id="image"
                               name="image">
                        <span class="invalid-feedback d-block" role="alert">{{ $errors->first('image') }}</span>
                    </div>
                    <hr>
                    <div class="form-row justify-content-center my-2">
                        <input type="submit" class="btn btn-primary icon-bg mr-3" value="Register">
                        <pre>  </pre>
                        <input type="reset" class="btn btn-secondary" value="Reset">
                    </div>
                </form>
                <p class="card-text"><strong class="text-muted">Have an account? <a
                            href="{{ route('login') }}">Login</a> here</strong></p>
                <hr>
                <p class="row justify-content-center text-bold" style="color: black">OR</p>
                <div class="row justify-content-center">
                    <social-login></social-login>
                </div>
            </div>
        </div>
    </div>

@endsection

