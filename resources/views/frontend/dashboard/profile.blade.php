@extends('frontend.dashboard.dashboard')

@section('dashboard')
    <div class="container pt-5">
        <h2>My Details</h2>
        <form action="{{route('profile')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <label for="email" class="col-form-label col-2">Email: </label>
                <div class="col-6">
                    <input type="email"
                           class="form-control-plaintext"
                           name="email"
                           id="email"
                           value="{{Auth::user()->email}}"
                           disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-form-label col-2">Name: </label>
                <div class="col-4">
                    <input type="text"
                           class="form-control"
                           name="name"
                           id="name"
                           value="{{Auth::user()->name}} @if($errors->first('name')) is-invalid @endif"
                           placeholder="Your Name">
                </div>
                <span class="invalid-feedback d-block" role="alert">{{ $errors->first('name') }}</span>
            </div>
            <div class="form-group row">
                <label for="country" class="col-form-label col-2">Country: </label>
                <div class="col-4">
                    <input type="text"
                           class="form-control @if($errors->first('country')) is-invalid @endif"
                           name="country"
                           id="country"
                           value="{{Auth::user()->country}}"
                           placeholder="Country">
                </div>
                <span class="invalid-feedback d-block" role="alert">{{ $errors->first('country') }}</span>
            </div>
            <div class="form-group row">
                <label for="phone" class="col-form-label col-2">Phone: </label>
                <div class="col-4">
                    <input type="text"
                           class="form-control @if($errors->first('phone')) is-invalid @endif"
                           name="phone"
                           id="phone"
                           value="{{Auth::user()->phone}}"
                           placeholder="Your Phone Number">
                </div>
                <span class="invalid-feedback d-block" role="alert">{{ $errors->first('phone') }}</span>
            </div>
            <div class="form-group row">
                <label for="image" class="col-form-label col-2">Profile Picture: </label>
                <div class="col-4">
                    <input type="file"
                           class="form-control-file @if($errors->first('image')) is-invalid @endif"
                           name="image"
                           id="image"
                           value="{{Auth::user()->image}}"
                           placeholder="Your Phone Number">
                </div>
                <span class="invalid-feedback d-block" role="alert">{{ $errors->first('image') }}</span>
            </div>
            <div class="form-group row">
                <span class="d-block text-primary col-12"><i>If you want to change your password.</i></span>
                <label for="password" class="col-form-label col-2">Password: </label>
                <div class="col-4">
                    <input type="password"
                           class="form-control @if($errors->first('password')) is-invalid @endif"
                           name="password"
                           id="password"
                           placeholder="New Password">
                </div>
                <div class="col-4">
                    <input type="password"
                           class="form-control @if($errors->first('password')) is-invalid @endif"
                           name="password_confirmation"
                           placeholder="Confirm Password">
                </div>
                <span class="invalid-feedback d-block" role="alert">{{ $errors->first('password') }}</span>
            </div>
            <div class="form-group">
                <button type="submit" class="btn icon-bg text-white col-2">Update</button>
            </div>
        </form>
    </div>
@endsection
