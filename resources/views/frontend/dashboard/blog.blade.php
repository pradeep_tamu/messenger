@extends('frontend.dashboard.dashboard')

@section('dashboard')
    <div class="container pt-5">
        <h2>My Blogs</h2>
        <div class="mt-5">
            <a class="btn icon-bg text-white mb-5" href="{{route('profile.blog.create')}}"><span class="text-white">Create Blog</span></a>
            @if(count($blogs) > 0)
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th width="15%">Image</th>
                        <th width="45%">Title</th>
                        <th width="30%">Action: View | Edit | Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($blogs as $blog)
                        <tr>
                            <td>{{$loop->index + 1}}</td>
                            <td>
                                <img class="img-responsive center" style="height: 55px; width: 65px;"
                                     src="{{ asset('img/blog/'. $blog->image)}}" alt="blog">
                            </td>
                            <td>{{$blog->title}}</td>
                            <td align="center">
                                <div class="btn-group">
                                    <a title="View" href="{{ Route('blog.show',$blog->slug) }}"
                                       class="btn btn-info icon-bg btn-sm"><i
                                            class="fa fa-eye text-white"></i></a>
                                </div>
                                |
                                <div class="btn-group">
                                    <a title="Edit" href="{{ Route('profile.blog.edit', $blog->id) }}"
                                       class="btn btn-info icon-bg btn-sm"><i
                                            class="fa fa-edit text-white"></i></a>
                                </div>
                                |
                                <div class="btn-group">
                                    <a title="Edit" href="{{ Route('profile.blog.delete', $blog->id) }}"
                                       class="btn btn-danger btn-sm"><i
                                            class="fa fa-fw fa-trash text-white"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>
                    <strong>You don't hava written any blog yet.</strong>
                </p>
            @endif
        </div>
    </div>
@endsection
