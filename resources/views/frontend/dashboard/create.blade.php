@extends('frontend.dashboard.dashboard')

@section('dashboard')
    <div class="contact-form-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading -->
                    <div class="section-heading text-center wow fadeInUp" data-wow-delay="100ms">
                        <h4 class="text-body">Create Your Blog</h4>
                        <h2>Express your experiences...</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <!-- Form -->

                    <div class="contact-form">
                        <form action="{{ route('profile.blog.create') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row justify-content-center">
                                <div class="col-12 col-lg-4" style="margin-left: 30%; margin-right: 30%; width: 40%;">
                                    <img class="mb-15" id="imagePreview" style="height: 15rem; width: 100%">
                                </div>
                                <div class="col-12 col-lg-3" style="margin-left: 40%; margin-right: 40%; width: 20%;">
                                    <input type="file"
                                           onchange="loadFile(event)"
                                           class="form-control-file mb-30"
                                           name="image" value="{{ old('image') }}"
                                           required>
                                    <span class="invalid-feedback d-block"
                                          role="alert">{{ $errors->first('image') }}</span>
                                </div>
                                <div class="col-12 col-lg-8">
                                    <input type="text"
                                           name="title"
                                           class="form-control mb-30"
                                           placeholder="Blog Title"
                                           value="{{ old('title') }}" required>
                                    <span class="invalid-feedback d-block"
                                          role="alert">{{ $errors->first('title') }}</span>
                                </div>
                                <div class="col-12">
                                    <textarea name="body"
                                              id="body"
                                              class="form-control mb-30"
                                              placeholder="Your Experience...."
                                              required>{{ old('body') }}</textarea>
                                    <span class="invalid-feedback d-block"
                                          role="alert">{{ $errors->first('body') }}</span>
                                </div>

                                <div class="col-12 text-center ">
                                    <button type="submit" class="btn icon-bg text-white mt-3">Create</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

