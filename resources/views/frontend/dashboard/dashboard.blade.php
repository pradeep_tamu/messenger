@extends('frontend.layout.layout')

@section('title', 'Profile | Messanger Tours adn Travel')
@section('description', 'Messanger Tours and Travel specialize in organizing outdoor activities such as heritage/cultural and pilgrimage
                            tours, trekking, peak climbing, mountain expedition, white-water rafting, jungle safari,
                            paragliding, bungee jump, and many other adventure sports in Nepal with outbound tours
                            to Tibet (China), Bhutan and India (Darjeeling / Sikkim / Ladakh). Each itinerary is
                            well-researched and developed to suit any particular interest and requirement of our
                            clients.')
@section('keywords', 'tours,travel,travel agency')

@section('content')
    <div class="hold-transition sidebar-mini">
        <div class="wrapper">

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-light-secondary elevation-1" style="z-index: 1">
                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img
                                src="@if(Auth::user()->image ){{ asset('/img/webuser/'. Auth::user()->image) }} @else {{ asset('img/profile.jpg')}} @endif"
                                class="img-circle elevation-2" alt="user image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block">{{Auth::user()->name}}</a>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu">
                            <li class="nav-item">
                                <a href="{{route('profile')}}" class="nav-link">
                                    <i class="nav-icon fas fa-user-circle"></i>
                                    <p>
                                        User Info
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('profile.blog')}}" class="nav-link">
                                    <i class="nav-icon fas fa-blog"></i>
                                    <p>
                                        My Blog
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-clipboard-list"></i>
                                    <p>
                                        My Bookings
                                    </p>
                                </a>
                            </li>
                        </ul>

                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            <div class="content-wrapper bg-white">
                @yield('dashboard')
            </div>
        </div>
    </div>

    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
    <script>
        let loadFile = function (event) {
            let reader = new FileReader();
            reader.onload = function () {
                let output = document.getElementById('imagePreview');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };
        CKEDITOR.replace('body');
    </script>
@endsection
