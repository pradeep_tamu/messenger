@extends('frontend.dashboard.dashboard')

@section('dashboard')
    <div class="contact-form-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading -->
                    <div class="section-heading text-center wow fadeInUp" data-wow-delay="100ms">
                        <h5>Create Your Blog</h5>
                        <h2>Express your experiences...</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <!-- Form -->

                    <div class="contact-form">
                        <form action="{{ route('profile.blog.edit', $blog->id) }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="row justify-content-center">
                                <div class="col-12 col-lg-4" style="margin-left: 30%; margin-right: 30%; width: 40%;">
                                    <img class="mb-15" id="imagePreview" src="/img/blog/{{$blog->image}}"
                                         style="height: 15rem; width: 100%">
                                </div>
                                <div class="col-12 col-lg-3" style="margin-left: 40%; margin-right: 40%; width: 20%;">
                                    <input type="file" onchange="loadFile(event)" class="form-control-file mb-30"
                                           name="image">
                                    <span class="invalid-feedback d-block"
                                          role="alert">{{ $errors->first('image') }}</span>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <input type="text" name="title" class="form-control mb-30" placeholder="Blog Title"
                                           value="{{ $blog->title }}" required>
                                    <span class="invalid-feedback d-block"
                                          role="alert">{{ $errors->first('title') }}</span>
                                </div>
                                <div class="col-12">
                                <textarea name="body" id="body" class="form-control mb-30"
                                          placeholder="Your Experience...." required>{{ $blog->body }}</textarea>
                                    <span class="invalid-feedback d-block"
                                          role="alert">{{ $errors->first('body') }}</span>
                                </div>

                                <div class="col-12 text-center ">
                                    <button type="submit" class="btn icon-bg text-white mt-3">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
