require('./bootstrap');
require('./notes');

window.Vue = require('vue');

import VueAxios from 'vue-axios'
import axios from 'axios';

Vue.use(VueAxios, axios);

import VueSocialauth from 'vue-social-auth'

Vue.use(VueSocialauth, {
    providers: {
        google: {
            clientId: '1060090086063-h05ujrgf5j6dh4osqhn6kn0p353hlgig.apps.googleusercontent.com',
            redirect_uri: 'https://localhost:8000/auth/google/callback' // Your client app URL
        },
        facebook: {
            clientId: '576013546539596',
            redirect_uri: 'https://localhost:8000/auth/facebook/callback/' // Your client app URL
        }
    }
});

import SocialLogin from './components/SocialLogin';

Vue.component('social-login', SocialLogin);

// // facebook messenger
import VueFbCustomerChat from 'vue-fb-customer-chat'

Vue.use(VueFbCustomerChat, {
    page_id: 406482306483614,
    theme_color: '#009cff', // theme color in HEX
    locale: 'en_US',
});
// // -- facebook messenger

const app = new Vue({
    el: '#app',
});









