<?php
require "frontend.php";

Route::group(['prefix' => 'reg@nessem/admin'], function () {

    Route::group(['namespace' => 'auth'], function () {
        Route::get('/login', 'AdminAuthController@loginForm')->name('admin.login');
        Route::post('/login', 'AdminAuthController@login');
        Route::post('/logout', 'AdminAuthController@logout')->name('admin.logout');
    });

    Route::group(['namespace' => 'backend'], function () {
        //main admin dashboard
        Route::group(['middleware' => 'admin'], function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');

            // User
            Route::group(['prefix' => 'users'], function () {
                Route::get('/user', 'UserController@user')->name('user.list');
                Route::get('/user/activate/{id}', 'UserController@activate')->name('user.activate');
                Route::get('/user/block/{id}', 'UserController@block')->name('user.block');
                Route::get('/admin', 'UserController@admin')->name('admin.list');
                Route::post('destroy/{id}', 'UserController@destroy')->name('user.destroy');
            });

            //Destination Route
            Route::group(['prefix' => 'destinations'], function () {
                Route::get('/', 'DestinationController@index')->name('destinations.list');
                Route::get('create', 'DestinationController@create')->name('destinations.create');
                Route::post('create', 'DestinationController@store');
                Route::get('update/{id}', 'DestinationController@edit')->name('destinations.edit');
                Route::patch('update/{id}', 'DestinationController@update');
                Route::post('destroy/{id}', 'DestinationController@destroy')->name('destinations.destroy');
            });

            //Activities and Adventures Routes
            Route::group(['prefix' => 'activity'], function () {
                Route::get('/', 'ActivitiesController@index')->name('activity.list');
                Route::get('create', 'ActivitiesController@create')->name('activity.create');
                Route::post('create', 'ActivitiesController@store');
                Route::get('update/{id}', 'ActivitiesController@edit')->name('activity.edit');
                Route::patch('update/{id}', 'ActivitiesController@update');
                Route::post('destroy/{id}', 'ActivitiesController@destroy')->name('activity.destroy');
            });

            //Packages Routes
            Route::group(['prefix' => 'packages'], function () {
                Route::get('/', 'PackageController@index')->name('package.list');
                Route::get('view/{id}', 'PackageController@view')->name('package.view');
                Route::get('create', 'PackageController@create')->name('package.create');
                Route::post('create', 'PackageController@store');
                Route::get('update/{id}', 'PackageController@edit')->name('package.edit');
                Route::patch('update/{id}', 'PackageController@update');
                Route::post('destroy/{id}', 'PackageController@destroy')->name('package.destroy');
                Route::get('destination', 'PackageController@getPackages')->name('package.destination');
            });

            //Enquiry
            Route::resource('enquiry', 'EnquiryController');
            Route::group(['prefix' => 'blog'], function () {
                Route::get('/', 'BlogController@list')->name('blog.list');
                Route::post('/delete/{id}', 'BlogController@delete')->name('blog.destroy');
            });

            //Bookings
            Route::group(['prefix' => 'booking-list'], function () {
                Route::get('/package', 'BookingController@package')->name('booking.package.list');
                Route::get('/adventure', 'BookingController@adventure')->name('booking.adventure.list');
                Route::get('/booking_detail/{id}', 'BookingController@bookingDetail')->name('booking.detail');
                Route::get('/booking_status/{id}', 'BookingController@status')->name('booking.status');
                Route::post('destroy/{id}', 'BookingController@destroy')->name('booking.destroy');
            });

            //Landing Slider
            Route::resource('slider', 'ImageSliderController');
        });
    });
});
