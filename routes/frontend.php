<?php

// Data required by all views. Navigation Data
use App\Activities;
use App\Destination;
use App\Package;

View::composer(['*frontend.*'], function ($view) {
    $nav_destinations = Destination::orderBy('name', 'asc')->where('slug', '!=', 'tibet')->where('slug', '!=', 'bhutan')->get();
    $nav_packages = Package::orderBy('name', 'asc')->select('name', 'slug')->get();
    $nav_adventures = Activities::orderBy('name', 'asc')->select('name', 'slug')->get();
    $tibet = Destination::where('slug', 'tibet')->select('id')->first();
    $tibet_packages = Package::where('destination_id', $tibet->id)->orderBy('name', 'asc')->select('name', 'slug')->get();
    $bhutan = Destination::where('slug', 'bhutan')->select('id')->first();
    $bhutan_packages = Package::where('destination_id', $bhutan->id)->orderBy('name', 'asc')->select('name', 'slug')->get();

    $view->with('nav_destinations', $nav_destinations)
        ->with('nav_packages', $nav_packages)
        ->with('nav_adventures', $nav_adventures)
        ->with('tibet_packages', $tibet_packages)
        ->with('bhutan_packages', $bhutan_packages);
});

// Login and Register
Route::group(['namespace' => 'Auth'], function () {
    Route::group(['middleware' => 'ifnotloggedin'], function () {
        Route::post('socialLogin/{provider}', 'SocialLoginController@socialSignUp');
        Route::get('/login', 'AuthController@loginForm')->name('login');
        Route::post('/login', 'AuthController@login');
        Route::get('/register', 'AuthController@registerForm')->name('register');
        Route::post('/register', 'AuthController@register');
        Route::any('/register/activate/{token}', 'AuthController@signUpActivate');

        // Password reset
        Route::get('/password', 'PasswordController@requestForm')->name('password.request');
        Route::post('/password', 'PasswordController@request');
        Route::get('/password/{token}', 'PasswordController@resetForm');
        Route::post('/password-reset', 'PasswordController@reset')->name('password.reset');
    });

    Route::group(['middleware' => 'ifloggedin'], function () {
        Route::get('/logout', 'AuthController@logout')->name('logout');
    });
});


Route::group(['namespace' => 'frontend'], function () {

    // Home
    Route::get('/', 'PagesController@landing')->name('home');

    // Destinations
    Route::get('/destination', 'DestinationController@allDestinations')->name('destination');
    Route::get('/destination/{slug}', 'PackageController@showPackages')->name('destination.package');

    // Package
    Route::get('/package', 'PackageController@allPackages')->name('package');
    Route::get('/package/{slug}', 'PackageController@singlePackage')->name('package.show');

    // Adventures
    Route::get('/adventure', 'AdventureController@allAdventures')->name('adventure');
    Route::get('/adventure/{slug}', 'AdventureController@singleAdventure')->name('adventure.show');

    // Blog
    Route::get('/blog', 'BlogController@index')->name('blog');
    Route::get('/blog/{slug}', 'BlogController@show')->name('blog.show');

    // Pages
    Route::get('/about', 'PagesController@aboutNavigation')->name('about');
    Route::get('/contact', 'PagesController@contactNavigation')->name('contact');

    // Enquiry
    Route::post('/enquiry', 'EnquiryController@store')->name('enquiry');

    // Search
    Route::post('/fetch-search-package', 'SearchController@searchPackage')->name('package.fetch');
    Route::post('/fetch-search-destination', 'SearchController@searchDestination')->name('destination.fetch');
    Route::post('/fetch-search-adventure', 'SearchController@searchAdventure')->name('adventure.fetch');

    // Search home
    Route::post('/show-available-package', 'SearchController@showAvailablePackage')->name('package.available');
    Route::post('/show-package-duration', 'SearchController@showPackageDuration')->name('package.duration');

    // Privacy
    Route::get('/privacy-policy', 'PagesController@privacy')->name('privacy');
    Route::get('/why-nepal', 'PagesController@nepal')->name('nepal');
    Route::get('/why-bhutan', 'PagesController@bhutan')->name('bhutan');
    Route::get('/why-tibet', 'PagesController@tibet')->name('tibet');

    // Bookings
    Route::get('/booking/{type}/{slug}', 'BookingController@index')->name('booking');
    Route::post('/booking', 'BookingController@store')->name('booking.store');

    // functions enabled after logged In
    Route::group(['middleware' => 'ifloggedin'], function () {

        Route::group(['prefix' => 'profile'], function () {
            // User Profile
            Route::get('', 'UserController@profile')->name('profile');
            Route::post('', 'UserController@update');

            Route::get('/blog', 'BlogController@blog')->name('profile.blog');
            Route::get('/blog/create', 'BlogController@create')->name('profile.blog.create');
            Route::post('/blog/create', 'BlogController@store');
            Route::get('/blog/edit/{id}', 'BlogController@edit')->name('profile.blog.edit');
            Route::post('/blog/edit/{id}', 'BlogController@update');
            Route::post('/blog/delete/{id}', 'BlogController@destroy')->name('profile.blog.delete');
        });

        Route::post('/review', 'ReviewController@review')->name('package.review');
        Route::post('/review/{id}', 'ReviewController@reviewEdit')->name('package.review.edit');

    });

});

