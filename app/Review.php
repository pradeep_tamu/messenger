<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'reviews';

    public function package(){
        return $this->belongsTo('App\Package', 'package_id');
    }
}
