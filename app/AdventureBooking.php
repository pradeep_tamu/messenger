<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdventureBooking extends Model
{
    protected $table = 'adventurebookings';
    protected $fillable = [
        'id',
        'adventure_id',
        'user_id',
        'name',
        'email',
        'country',
        'mobileNo',
        'landlineNo',
        'dob',
        'mailingAddress',
        'passportNumber',
        'placeOfIssue',
        'issueDate',
        'insurance',
        'occupation',
        'expirationDate',
        'status'
    ];

    public function adventure()
    {
        return $this->belongsTo('App\Activities');
    }
    public function websiteuser()
    {
        return $this->belongsTo('App\WebsiteUser');
    }
}
