<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    //
    protected $table = "abouts";

    public $primaryKey = "id";

    public $timestamps = true;
}
