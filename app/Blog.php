<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';
    protected $fillable = [
        'title',
        'slug',
        'image',
        'body',
        'users_id'
    ];

    public function writer()
    {
        return $this->belongsTo(User::class, 'users_id')
            ->select('id', 'name', 'image');
    }

}
