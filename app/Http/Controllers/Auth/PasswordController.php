<?php

namespace App\Http\Controllers\Auth;

use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class PasswordController extends Controller
{
    public function requestForm() {
        return view('frontend.auth.password.request');
    }

    public function request(Request $request){
        $request->validate([
            'email' => 'required|email|unique:password_resets'
        ]);

        $user = User::where('email', $request->email)->first();

        if(!$user){
            Session::flash('error', 'No account found. Invalid Email Address');
            return Redirect::back();
        }

        $password = PasswordReset::create([
            'email' => $request->email,
            'token' => Str::random(64)
        ]);

        $user->notify(new PasswordResetRequest($password->token));

        $message = 'Please check your email for password reset request.';
        Session::flash('message', $message);
        return Redirect::route('login');
    }

    public function resetForm($token) {
        return view('frontend.auth.password.reset', compact('token'));
    }

    public function reset(Request $request) {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6'
        ]);

        $password = PasswordReset::where('token', $request->token)->first();

        if(!$password) {
            Session::flash('error', 'There is no request for password reset associated with this email.');
            return Redirect::back();
        }

        $user = User::where('email', $password->email)->first();
        $user->password = bcrypt($request->password);
        $user->save();
        $password->delete();

        $user->notify(new PasswordResetSuccess());

        $message = 'Your password has been updated. You can login now.';
        Session::flash('message', $message);
        return Redirect::route('login');
    }
}
