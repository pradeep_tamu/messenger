<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AdminAuthController extends Controller
{
    public function loginForm()
    {
        return view('backend.auth.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        Auth::logout();

        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ])) {
            if (Auth::user()->type == 'admin') {
                return Redirect::route('dashboard');
            } else {
                Auth::logout();
                Session::flash('error', 'You are not an admin.');
                return Redirect::back();
            }
        }

        Session::flash('error', 'Invalid email address.');
        return Redirect::back();
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::route('admin.login');
    }
}
