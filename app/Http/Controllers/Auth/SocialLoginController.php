<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends Controller
{
    public function socialSignUp($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();

        $existingUser = User::where('email', $user->email)->first();

        if ($existingUser) {
            Auth::login($existingUser);
            return Redirect::route('home');
        } else {
            $existingUser = User::create([
                'name' => $user->name,
                'email' => $user->email,
                'active' => 1
            ]);
            Auth::login($existingUser);
            return Redirect::route('home');
        }

    }

}
