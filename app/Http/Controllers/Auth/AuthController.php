<?php

namespace App\Http\Controllers\Auth;

use App\Notifications\ActivateAccount;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class AuthController extends Controller
{
    public function loginForm()
    {
        return view('frontend.auth.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ])) {
            if (Auth::user()->type == 'user') {
                if (Auth::user()->active == true) {
                    return Redirect::route('home');
                } else {
                    Auth::logout();
                    Session::flash('error', 'You account has not been yet activated.');
                    return Redirect::back();
                }
            } else {
                Auth::logout();
                Session::flash('error', 'You are not a user.');
                return Redirect::back();
            }
        }

        Session::flash('error', 'Invalid email address.');
        return Redirect::back();
    }

    public function logout() {
        Auth::logout();
        return Redirect::route('home');
    }

    public function registerForm(){
        return view('frontend.auth.register');
    }

    public function register(Request $request){
        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'country' => 'required|min:3',
            'phone' => 'required|min:7',
        ]);

        if ($request->image) {
            $image = $request->file('image');
            $imageName = md5(microtime()) . '.' . $image->getClientOriginalExtension();
            Image::make($image)
                ->save(public_path('/img/user/') . $imageName);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'country' => $request->name,
            'phone' => $request->name,
            'image' => $imageName,
            'token' => Str::random(64)
        ]);

        $user->notify(new ActivateAccount());

        $message = 'Thank you for registration. Please confirm your email before login';
        Session::flash('message', $message);
        return Redirect::route('login');

    }

    public function signUpActivate($token)
    {
        $user = User::where('token', $token)->first();

        if (!$user) {
            $message = 'This activation token is invalid.';
            Session::flash('message', $message);

        }
        $user->active = true;
        $user->email_verified_at = Carbon::now();
        $user->token = '';
        $user->save();

        $message = 'Account Activated. Now you can login';
        Session::flash('message', $message);
        return Redirect::route('login');
    }

}
