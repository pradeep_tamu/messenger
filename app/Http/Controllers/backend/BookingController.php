<?php

namespace App\Http\Controllers\backend;

use App\AdventureBooking;
use App\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingController extends Controller
{
    public function package()
    {
        $bookings=Booking::whereNotNull('package_id')->get();
        return view('backend.booking.package',compact('bookings'));
    }

    public function adventure()
    {
        $bookings=Booking::whereNotNull('adventure_id')->get();
        return view('backend.booking.adventure',compact('bookings'));
    }

    public function bookingDetail($id){
        $booking=Booking::where('id',$id)->first();
        return view('backend.booking.bookingDetail',compact('booking'));
    }

    public function status($id){
        $booking =  Booking::findOrFail($id);
        $booking->status = true;
        $booking->save();
        return redirect()->route('booking.detail', $booking->id);
    }

    public function destroy($id){
        $booking =  Booking::findOrFail($id);
        $booking->delete();
        return redirect()->route('booking.list')->with('success', 'Deleted Successfully');
    }

}
