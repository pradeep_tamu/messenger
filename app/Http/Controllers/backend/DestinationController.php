<?php

namespace App\Http\Controllers\backend;

use App\Destination;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class DestinationController extends Controller
{
    public function index()
    {
        $destinations = Destination::get();
        return view('backend.destinations.destination', compact('destinations'));
    }

    public function create(){

        $destination = null;
        return view('backend.destinations.create', compact('destination'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|min:3|max:30',
            'description' => 'required|min:10',
            'image' => 'required|image|max:1999',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $images = $request->file('image');
        $extension = $images->getClientOriginalExtension();
        $imageName = str_random() . "." . $extension;
        Image::make($images)
            ->resize(360, 400)
            ->save(public_path('/img/destination/') . $imageName);
        Image::make($images)
            ->save(public_path('/img/thumbnail/destination/') . $imageName);


        $destination = new Destination();
        $destination->name = $request->name;
        $destination->description = $request->description;
        $destination->filename = $imageName;
        $destination->meta_title = $request->meta_title;
        $destination->meta_description = $request->meta_description;
        $destination->keyword = $request->keyword;
        $destination->slug = str_slug($request->name);

        $destination->save();

        return redirect()->route('destinations.list')->with('success', 'Destinations created successfully');
    }

    public function edit($id){
        $destination = Destination::where('id', $id)->first();
        return view('backend.destinations.create', compact('destination'));
    }

    public function update(Request $request, $id)
    {

        $destination = Destination::find($id);

        if (!$destination) {
            return redirect()->route('destinations.create')->with("error", 'no destinations');
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|min:3|max:30',
            'description' => 'required|min:10',
            'image' => 'image|max:1999',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        if ($request->file('image')) {
            $images = $request->file('image');

            $extension = $images->getClientOriginalExtension();
            $imageName = str_random() . "." . $extension;
            Image::make($images)
                ->resize(360, 400)
                ->save(public_path('/img/destination/') . $imageName);
            Image::make($images)
                ->resize(245, 162)
                ->save(public_path('/img/thumbnail/destination/') . $imageName);


            $destinationImg = public_path("/img/destination/" . $destination->filename);
            if (file_exists($destinationImg)) {
                @unlink($destinationImg);
            }
            $destination->filename = $imageName;
        }

        $destination->name = $request->name;
        $destination->description = $request->description;
        $destination->meta_title = $request->meta_title;
        $destination->meta_description = $request->meta_description;
        $destination->keyword = $request->keyword;
        $destination->slug = str_slug($request->name);

        $destination->save();

        return redirect()->route('destinations.list')->with('success', 'Destinations created successfully');
    }

    public function destroy($id){

        $destinaton =  Destination::findOrFail($id);
        $destinatonImg = public_path("/img/destination/" . $destinaton->filename);
        if (file_exists($destinatonImg)) {
            @unlink($destinatonImg);

        }
        $destinaton->delete();
        return redirect()->route('destinations.list')->with('success', ' Destination deleted.');
    }
}
