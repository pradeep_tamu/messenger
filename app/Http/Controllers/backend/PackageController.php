<?php

namespace App\Http\Controllers\backend;

use App\Destination;
use App\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class PackageController extends Controller
{
    public function index()
    {
        $packages = Package::get();
        return view('backend.package.package', compact('packages'));
    }

    public function view($id)
    {
        $package = Package::where('id', $id)->first();
        return view('backend.package.view', compact('package'));
    }

    public function getPackages(Request $request)
    {
        $packages = Package::where('destination_id', '=', $request->id)->get();
        return response()->json($packages);
    }

    public function create()
    {
        $destinations = Destination::pluck('name', 'id');
        $destination = null;
        $package = null;
        return view('backend.package.create', compact('package', 'destination', 'destinations'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'description' => 'required|min:10',
            'overview' => 'required|min:5',
            'itineraries' => 'required|min:5',
            'cost_details' => 'required|min:5',
            'practical_info' => 'required|min:5',
            'cost' => 'numeric',
            'image' => 'image|max:1999'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $package = new Package();
        $images = $request->file('image');
        $extension = $images->getClientOriginalExtension();
        $imageName = str_random() . "." . $extension;
        Image::make($images)
            ->resize(375, 230)
            ->save(public_path('/img/thumbnail/package/') . $imageName);
        Image::make($images)
            ->save(public_path('/img/package/') . $imageName);

        $package->meta_title = $request->meta_title;
        $package->meta_description = $request->meta_description;
        $package->keyword = $request->keyword;
        $package->name = $request->name;
        $package->duration = $request->duration;
        $package->cost = $request->cost;
        $package->capacity = $request->capacity;
        $package->destination_id = $request->destination_id;
        $package->itineraries = $request->itineraries;
        $package->practical_info = $request->practical_info;
        $package->cost_details = $request->cost_details;
        $package->overview = $request->overview;
        $package->description = $request->description;
        $package->filename = $imageName;
        $package->slug = str_slug($request->meta_title);

        $package->save();

        return redirect()->route('package.list')->with('success', 'Destinations created successfully');
    }

    public function edit($id)
    {
        $destinations = Destination::pluck('name', 'id');
        $destination = null;
        $package = Package::where('id', $id)->first();
        return view('backend.package.create', compact('package', 'destination', 'destinations'));
    }

    public function update(Request $request, $id)
    {
        $package = Package::find($id);
        if (!$package) {
            return redirect()->route('package.create')->with("error", 'no package');
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'description' => 'required|min:10',
            'overview' => 'required|min:5',
            'itineraries' => 'required|min:5',
            'cost_details' => 'required|min:5',
            'practical_info' => 'required|min:5',
            'cost' => 'numeric',
            'image' => 'image|max:1999'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        if ($request->file('image')) {
            $images = $request->file('image');

            $extension = $images->getClientOriginalExtension();
            $imageName = str_random() . "." . $extension;
            Image::make($images)
                ->resize(375, 230)
                ->save(public_path('/img/thumbnail/package/') . $imageName);
            Image::make($images)
                ->save(public_path('/img/package/') . $imageName);


            $packageImg = public_path("/img/package/" . $package->filename);
            if (file_exists($packageImg)) {
                @unlink($packageImg);

            }
            $packageImg = public_path("/img/thumbnail/package/" . $package->filename);
            if (file_exists($packageImg)) {
                @unlink($packageImg);
            }
            $package->filename = $imageName;
        }

        $package->meta_title = $request->meta_title;
        $package->meta_description = $request->meta_description;
        $package->keyword = $request->keyword;
        $package->name = $request->name;
        $package->duration = $request->duration;
        $package->cost = $request->cost;
        $package->capacity = $request->capacity;
        $package->destination_id = $request->destination_id;
        $package->itineraries = $request->itineraries;
        $package->practical_info = $request->practical_info;
        $package->cost_details = $request->cost_details;
        $package->overview = $request->overview;
        $package->description = $request->description;
        $package->slug = str_slug($request->meta_title);

        $package->save();

        return redirect()->route('package.list')->with('success', 'Packages updated successfully');
    }

    public function destroy($id)
    {
        $package = Package::findOrFail($id);
        $packageImg = public_path("/img/package/" . $package->filename);
        if (file_exists($packageImg)) {
            @unlink($packageImg);

        }
        $packageImg = public_path("/img/thumbnail/package/" . $package->filename);
        if (file_exists($packageImg)) {
            @unlink($packageImg);
        }
        $package->delete();
        return redirect()->route('package.list')->with('success', ' Package deleted.');
    }
}
