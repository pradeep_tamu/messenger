<?php

namespace App\Http\Controllers\backend;

use App\Activities;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;


class ActivitiesController extends Controller
{
    public function index()
    {
        $activities = Activities::get();
        return view('backend.activity.show', compact('activities'));
    }

    public function create(){

        $activity = null;
        return view('backend.activity.create', compact('activity'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'description' => 'required|min:3',
            'cost' => 'numeric',
            'image' => 'image|max:1999',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $images = $request->file('image');
        $extension = $images->getClientOriginalExtension();
        $imageName = str_random() . "." . $extension;
        Image::make($images)
            ->resize(400, 265)
            ->save(public_path('/img/thumbnail/adventure/') . $imageName);
        Image::make($images)
            ->save(public_path('/img/adventure/') . $imageName);


        $activity = new Activities();
        $activity->name = $request->name;
        $activity->description = $request->description;
        $activity->location = $request->location;
        $activity->cost = $request->cost;
        $activity->videos = $request->videos;
        $activity->season = $request->season;
        $activity->transportation = $request->transportation;
        $activity->start_end = $request->start_end;
        $activity->filename = $imageName;
        $activity->meta_title = $request->meta_title;
        $activity->meta_description = $request->meta_description;
        $activity->keyword = $request->keyword;
        $activity->slug = str_slug($request->meta_title);

        $activity->save();

        return redirect()->route('activity.list')->with('success', 'Destinations created successfully');
    }

    public function edit($id){
        $activity = Activities::where('id', $id)->first();
        return view('backend.activity.create', compact('activity'));
    }

    public function update(Request $request, $id)
    {

        $activity = Activities::find($id);
        if (!$activity) {
            return redirect()->route('activity.create')->with("error", 'no activity');
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'description' => 'required|min:3',
            'cost' => 'numeric',
            'image' => 'image|max:1999',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        if ($request->file('image')) {
            $images = $request->file('image');

            $extension = $images->getClientOriginalExtension();
            $imageName = str_random() . "." . $extension;
            Image::make($images)
                ->resize(400, 265)
                ->save(public_path('/img/thumbnail/adventure/') . $imageName);
            Image::make($images)
                ->save(public_path('/img/adventure/') . $imageName);


            $adventureImg = public_path("/img/adventure/" . $activity->filename);
            if (file_exists($adventureImg)) {
                @unlink($adventureImg);

            }
            $adventureImg = public_path("/img/thumbnail/adventure/" . $activity->filename);
            if (file_exists($adventureImg)) {
                @unlink($adventureImg);
            }
            $activity->filename = $imageName;
        }

        $activity->name = $request->name;
        $activity->description = $request->description;
        $activity->location = $request->location;
        $activity->cost = $request->cost;
        $activity->videos = $request->videos;
        $activity->season = $request->season;
        $activity->transportation = $request->transportation;
        $activity->start_end = $request->start_end;
        $activity->meta_title = $request->meta_title;
        $activity->meta_description = $request->meta_description;
        $activity->keyword = $request->keyword;
        $activity->slug = str_slug($request->meta_title);

        $activity->save();

        return redirect()->route('activity.list')->with('success', 'Activity updated successfully');
    }

    public function destroy($id){

        $activity =  Activities::findOrFail($id);
        $adventureImg = public_path("/img/adventure/" . $activity->filename);
        if (file_exists($adventureImg)) {
            @unlink($adventureImg);

        }
        $adventureImg = public_path("/img/thumbnail/adventure/" . $activity->filename);
        if (file_exists($adventureImg)) {
            @unlink($adventureImg);
        }
        $activity->delete();
        return redirect()->route('activity.list')->with('success', ' Activity deleted.');
    }
}
