<?php

namespace App\Http\Controllers\backend;
use App\Activities;
use App\AdventureBooking;
use App\Blog;
use App\Booking;
use App\Destination;
use App\Enquiry;
use App\Http\Controllers\Controller;
use App\Package;
use App\User;
use App\WebsiteUser;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $destinations = Destination::all();
        $adventures = Activities::all();
        $packages = Package::all();
        $enqueries = Enquiry::all();
        $webusers = User::all();
        $blogs = Blog::all();
        $packageBookings = Booking::whereNotNull('package_id')->get();
        $adventureBookings = Booking::whereNotNull('adventure_id')->get();

        return view('backend.dashboard.dashboard',
            compact('destinations',
            'adventures',
            'packages',
            'enqueries',
            'webusers',
            'blogs',
            'packageBookings',
            'adventureBookings'
        ));
    }
}
