<?php

namespace App\Http\Controllers\backend;

use App\Blog;
use App\WebsiteUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function list(){
        $blogs = DB::table('users')
            ->join('blogs', 'users.id', '=', 'blogs.users_id')
            ->select('users.name as writer', 'blogs.*')
            ->get();
        return view('backend.blog.blog')->with('blogs', $blogs);
    }

    public function delete($id){

        $blog = Blog::find($id);
        $blogImg = public_path("/img/blog/" . $blog->image);
        if (file_exists($blogImg)) {
            @unlink($blogImg);
        }
        $blogImg = public_path("/img/thumbnail/blog/" . $blog->image);
        if (file_exists($blogImg)) {
            @unlink($blogImg);
        }
        $blog->delete();

        return redirect()->route('blog.list');
    }
}
