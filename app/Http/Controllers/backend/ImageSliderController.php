<?php

namespace App\Http\Controllers\backend;

use App\ImageSlider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ImageSliderController extends Controller
{

    public function index()
    {
        $sliders = ImageSlider::all();
        return view('backend.slider.slider')->with('sliders', $sliders);
    }


    public function create()
    {
        return view('backend.slider.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|max:1999',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $images = $request->file('image');
        $extension = $images->getClientOriginalExtension();
        $imageName = str_random() . "." . $extension;
        Image::make($images)
            ->save(public_path('/img/slider/') . $imageName);

        $slider = new ImageSlider();
        $slider->filename = $imageName;

        $slider->save();

        Session::flash('message', 'Image added');
        return redirect()->route('slider.index');
    }

    public function destroy($id)
    {
        $image = ImageSlider::find($id);
        $sliderImg = public_path("/img/destination/" . $image->filename);
        if (file_exists($sliderImg)) {
            @unlink($sliderImg);
        }
        $image->delete();
        Session::flash('message', 'Image deleted');
        return redirect()->route('slider.index');
    }
}
