<?php

namespace App\Http\Controllers\backend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function user()
    {
        $users = User::where('type', 'user')->get();

        return view('backend.user.user', compact('users'));
    }

    public function activate($id)
    {
        $user = User::findOrFail($id);
        $user->active = true;
        $user->save();
        return redirect()->route('user.list');
    }

    public function block($id)
    {
        $user = User::findOrFail($id);
        $user->active = false;
        $user->save();
        return redirect()->route('user.list');
    }

    public function admin()
    {
        $users = User::where('type', 'admin')->get();

        return view('backend.user.admin', compact('users'));
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();
        return redirect()->route('user.list');
    }
}
