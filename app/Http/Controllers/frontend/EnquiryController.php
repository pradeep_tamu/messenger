<?php

namespace App\Http\Controllers\frontend;

use App\Enquiry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class EnquiryController extends Controller
{

    public function store(Request $request)
    {

        //validation
        $validatedData = $request->validate([
            'name' => 'required|min:3|max:20',
            'subject' => 'required|min:3|max:20',
            'email' => 'required|email|max:255',
            'message' => 'required|max:500|min:10'
        ]);

        if (!$validatedData) {
            return redirect()->back()->withErrors('errors', $validatedData);
        }

        // create new record of enquiry
        Enquiry::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'subject' => $request->input('subject'),
            'message' => $request->input('message'),
        ]);

        Mail::send('mails.enquiry_form',
            array(
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
                'enquiry_message' => $request->message
            ), function ($message) {
                $message->from('msntraveltours@gmail.com', 'Messangaer Tours and Travel');
                $message->to('msntraveltours@gmail.com', 'Messangaer Tours and Travel')->subject('Enquiry Received');
            });

        $message = 'Enquiry has been sent. We will contact you as soon as possible.';
        Session::flash('message', $message);
        return redirect('/contact');
    }


}
