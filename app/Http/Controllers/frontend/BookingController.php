<?php

namespace App\Http\Controllers\frontend;

use App\Activities;
use App\Booking;
use App\Http\Controllers\Controller;
use App\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class BookingController extends Controller
{
    public function index($type, $slug)
    {

        $package = ['id' => '', 'name' => ''];
        $adventure = ['id' => '', 'name' => ''];

        if ($type == 'package') {
            $package = Package::where('slug', $slug)->select('id', 'name')->first();
        } else {
            $adventure = Activities::where('slug', $slug)->select('id', 'name')->first();
        }

        return view('frontend.booking.booking', compact('type', 'package', 'adventure'));
    }

    public function store(Request $request)
    {
//        $request->validate([
//            'trip_start_date' => 'required|after:today',
//            'number_of_people' => 'required',
//            'first_name' => 'required|min:3',
//            'last_name' => 'required|min:3',
//            'dob' => 'required|before:today',
//            'email' => 'required|email',
//            'phone' => 'required|min:7',
//            'country' => 'required|min:3',
//            'passport_number' => 'required',
//            'passport_expiration_date' => 'required',
//            'mailing_address' => 'required',
//            'emergency_contact_name' => 'required|min:3',
//            'emergency_contact_relation' => 'required|min:3',
//            'emergency_contact_phone' => 'required|min:7',
//            'arrival_date' => 'required|date|after:today',
//            'arrival_time' => 'required',
//            'arrival_flight_number' => 'required',
//            'pickup' => 'required',
//            'departure_date' => 'required|after:arrival_date',
//            'departure_time' => 'required',
//            'departure_flight_number' => 'required',
//            'drop_off' => 'required',
//            'insurance' => 'required',
//            'reference' => 'required'
//        ]);

        $booking = Booking::create([
            'trip_start_date' => $request->trip_start_date,
            'number_of_people' => $request->number_of_people,
            'package_id' => $request->package_id,
            'adventure_id' => $request->adventure_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'dob' => $request->dob,
            'email' => $request->email,
            'phone' => $request->phone,
            'country' => $request->country,
            'passport_number' => $request->passport_number,
            'passport_expiration_date' => $request->passport_expiration_date,
            'mailing_address' => $request->mailing_address,
            'emergency_contact_name' => $request->emergency_contact_name,
            'emergency_contact_relation' => $request->emergency_contact_relation,
            'emergency_contact_phone' => $request->emergency_contact_phone,
            'arrival_date' => $request->arrival_date,
            'arrival_time' => $request->arrival_time,
            'arrival_flight_number' => $request->arrival_flight_number,
            'pickup' => $request->pickup,
            'departure_date' => $request->departure_date,
            'departure_time' => $request->departure_time,
            'departure_flight_number' => $request->departure_flight_number,
            'drop_off' => $request->drop_off,
            'insurance' => $request->insurance,
            'message' => $request->message,
            'reference' => $request->reference
        ]);

        $email = $request->email;

        $array = [
            'booking' => $booking
        ];

        if($request->has('package_id')){
            $package = Package::where('id', $request->package_id)->select('id', 'name')->first();
            $array['package'] = $package;
        } else {
            $adventure = Activities::where('id', $request->adventure_id)->select('id', 'name')->first();
            $array['adventure'] = $adventure;
        }

        $array['msg'] = "You have received a booking from " . $request->email;
        Mail::send('mails.booking',
            $array, function ($message) {
                $message->from('msntraveltours@gmail.com', 'Messangaer Tours and Travel');
                $message->to('msntraveltours@gmail.com', 'Messangaer Tours and Travel')->subject('Booking Received');
            });

        $array['msg'] = "Your booking is successful.";
        Mail::send('mails.booking',
            $array, function ($message) use ($email) {
                $message->from('msntraveltours@gmail.com', 'Messangaer Tours and Travel');
                $message->to($email)->subject('Booking Successful');
            });

        return Redirect::route('home');
    }

}
