<?php

namespace App\Http\Controllers\frontend;

use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class BlogController extends Controller
{
    //frontend blog list page
    public function index()
    {
        $blogs = DB::table('users')
            ->join('blogs', 'users.id', '=', 'blogs.users_id')
            ->select('users.name as writer', 'users.image as writer_photo', 'blogs.*')
            ->orderBy('created_at', 'desc')
            ->paginate(5);
        $recentblogs = DB::table('users')
            ->join('blogs', 'users.id', '=', 'blogs.users_id')
            ->select('users.name as writer', 'blogs.*')
            ->limit(5)
            ->get();
        return view('frontend.content.blog', compact('blogs', 'recentblogs'));
    }

    public function show($slug)
    {
        $blog = DB::table('users')
            ->join('blogs', 'users.id', '=', 'blogs.users_id')
            ->select('users.name as writer', 'users.image as writer_photo', 'blogs.*')
            ->where('blogs.slug', $slug)
            ->first();
        $recentblogs = DB::table('users')
            ->join('blogs', 'users.id', '=', 'blogs.users_id')
            ->select('users.name as writer', 'blogs.*')
            ->orderBy('created_at', 'desc')
            ->limit(5)
            ->get();
        return view('frontend.content.single_blog', compact('blog', 'recentblogs'));
    }

    public function blog()
    {
        $blogs = Blog::with('writer')
            ->where('users_id', '=', Auth::user()->id)
            ->get();

        return view('frontend.dashboard.blog', compact('blogs'));
    }

    public function create()
    {
        return view('frontend.dashboard.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|min:10',
            'body' => 'required',
            'image' => 'required|image'
        ]);

        $blog = new Blog();
        $imageName = $this->make($request->file('image'));

        $blog->title = $request->input('title');
        $blog->body = $request->input('body');
        $blog->image = $imageName;
        $blog->slug = Str::slug($request->input('title'));
        $blog->users_id = Auth::user()->id;

        $blog->save();

        Session::flash('message', 'Blog added');
        return Redirect::route('profile.blog');
    }

    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('frontend.dashboard.edit')->with('blog', $blog);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|min:10',
            'body' => 'required'
        ]);

        $blog = Blog::find($id);

        if ($request->file('image')) {
            $imageName = $this->make($request->file('image'));

            $blogImg = public_path("/img/blog/" . $blog->image);
            if (file_exists($blogImg)) {
                @unlink($blogImg);
            }
            $blogImg = public_path("/img/thumbnail/blog/" . $blog->image);
            if (file_exists($blogImg)) {
                @unlink($blogImg);
            }

            $blog->image = $imageName;
        }

        $blog->title = $request->input('title');
        $blog->body = $request->input('body');
        $blog->slug = Str::slug($request->input('title'));

        $blog->save();

        Session::flash('message', 'Blog edited');
        return Redirect::route('profile.blog');
    }

    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blogImg = public_path("/img/blog/" . $blog->image);
        if (file_exists($blogImg)) {
            @unlink($blogImg);
        }
        $blogImg = public_path("/img/thumbnail/blog/" . $blog->image);
        if (file_exists($blogImg)) {
            @unlink($blogImg);
        }
        $blog->delete();
        return Redirect::route('profile.blog');
    }

    public function make($image)
    {
        $extension = $image->getClientOriginalExtension();
        $name = md5(microtime()) . "." . $extension;
        Image::make($image)
            ->resize(375, 200)
            ->save(public_path('/img/thumbnail/blog/') . $name);
        Image::make($image)
            ->save(public_path('/img/blog/') . $name);

        return $name;
    }

}
