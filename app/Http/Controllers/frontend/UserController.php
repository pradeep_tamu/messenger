<?php

namespace App\Http\Controllers\frontend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    public function profile()
    {
        return view('frontend.dashboard.profile');
    }

    public function update(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);

        if ($request->image != $user->image) {
            $image = $request->file('image');
            $imageName = md5(microtime()) . '.' . $image->getClientOriginalExtension();
            Image::make($image)
                ->save(public_path('/img/user/') . $imageName);

            $user->image = $imageName;

            $profileImg = public_path("/img/user/" . $user->image);
            if (file_exists($profileImg)) {
                @unlink($profileImg);
            }
        }

        $user->name = $request->name;
        $user->country = $request->country;
        $user->phone = $request->phone;

        if ($user->password == null) {
            $request->validate([
                'password' => 'confirmed|min6'
            ]);
            $user->password = bcrypt($request->password);
        }

        $user->save();

        Session::flash('message', 'User edited');
        return Redirect::route('profile');
    }
}
