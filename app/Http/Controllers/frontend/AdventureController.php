<?php

namespace App\Http\Controllers\frontend;

use App\Activities;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AdventureController extends Controller
{
    //Adventure
    public function allAdventures()
    {
        $adventures = DB::table('activities')->orderBy('name', 'asc')->paginate(5);
        return view('frontend.content.adventure', compact('adventures'));
    }

    public function singleAdventure($slug)
    {
        $adventure = Activities::where('slug', $slug)->first();
        $related_adventure = DB::table('activities')->limit(3)->orderBy('name', 'asc')->get();
        return view('frontend.content.single_adventure', compact('related_adventure', 'adventure'));

    }
}
