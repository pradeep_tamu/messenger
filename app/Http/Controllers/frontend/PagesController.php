<?php

namespace App\Http\Controllers\frontend;

use App\Activities;
use App\Blog;
use App\Destination;
use App\Http\Controllers\Controller;
use App\ImageSlider;
use App\Package;
use App\User;
use Illuminate\Http\Request;
use willvincent\Rateable\Rating;
use App\Review;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{

    public function landing()
    {
        $packages = Package::limit(5)->get();
        $slides = ImageSlider::get();
        $destination_image = Destination::limit(4)->orderBy('name', 'asc')->get();

        $blogs = Blog::limit(3)->get();

        return view('frontend.landing', compact('destination_image', 'packages', 'blogs', 'slides'));
    }

    //about
    public function aboutNavigation()
    {
        return view('frontend.about');
    }

    //contact
    public function contactNavigation()
    {
        return view('frontend.contact');
    }

    //privacy
    public function privacy()
    {
        return view('frontend.privacy');
    }

    //nepal
    public function nepal()
    {
        return view('frontend.nepal');
    }

    //bhutan
    public function bhutan()
    {
        return view('frontend.bhutan');
    }

    //tibet
    public function tibet()
    {
        return view('frontend.tibet');
    }
}
