<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    // fetch package data
    public function searchPackage(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('packages')
                ->where('name', 'LIKE', "%{$query}%")
                ->get();
            echo json_encode($data);

        }
    }

    // fetch package data
    public function searchDestination(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('destinations')
                ->where('name', 'LIKE', "%{$query}%")
                ->get();
            echo json_encode($data);
        }
    }

    // fetch package data
    public function searchAdventure(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('activities')
                ->where('name', 'LIKE', "%{$query}%")
                ->get();
            echo json_encode($data);
        }
    }

    public function showAvailablePackage(Request $request)
    {
        $id = $request->get('query');
        $data = DB::table('packages')
            ->where('destination_id', '=', $id)
            ->get();
        echo json_encode($data);
    }

    public function showPackageDuration(Request $request)
    {
        $id = $request->get('query');
        $data = DB::table('packages')
            ->where('id', '=', $id)
            ->first();
        echo json_encode($data);
    }

}
