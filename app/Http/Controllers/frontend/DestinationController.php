<?php

namespace App\Http\Controllers\frontend;

use App\Destination;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DestinationController extends Controller
{
    //all destination list
    public function allDestinations()
    {
        $destinations = Destination::orderBy('name', 'asc')->get();

        return view('frontend.content.destination', compact('destinations'));
    }
}
