<?php

namespace App\Http\Controllers\frontend;

use App\Package;
use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use willvincent\Rateable\Rating;

class ReviewController extends Controller
{
    public function review(Request $request)
    {
        $validator = $request->validate([
            'review' => 'required|min:10|max:255',
        ]);

        if (!$validator) {
            return redirect()->back()->withErrors('errors', $validator);
        }

        if (Review::where('writer_id', '=', Auth::user()->id)->first()) {
            $review = Review::all()->where('writer_id', '=', Auth::user()->id)->first();
            $rating = Rating::where('user_id', '=', Auth::user()->id)->first();
        } else {

            $review = new Review();
            $rating = new Rating;
        }

        $package = Package::find($request->get('package_id'));

        $rating->rating = $request->get('rating');
        $rating->user_id = Auth::user()->id;
        $package->ratings()->save($rating);

        $review->review = $request->get('review');
        $review->package_id = $request->get('package_id');
        $review->writer_id = Auth::user()->id;
        $review->save();

        return redirect()->back();
    }

    public function reviewEdit($id, Request $request)
    {
        $validator = $request->validate([
            'review' => 'required|min:10|max:255',
        ]);

        if (!$validator) {
            return redirect()->back()->withErrors('errors', $validator);
        }

        $review = Review::find($id);
        $review->review = $request->get('review');
        $review->save();

        return redirect()->back();
    }
}
