<?php

namespace App\Http\Controllers\frontend;

use App\Destination;
use App\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use willvincent\Rateable\Rating;

class PackageController extends Controller
{
    //all list of packages
    public function allPackages()
    {
        $packages = DB::table('packages')->orderBy('name', 'asc')->paginate(5);
        $destination = 'null'; //destination for meta tag
        return view('frontend.content.package', compact('destination', 'packages'));
    }

    //full package description
    public function singlePackage($slug)
    {
        $package = Package::where('slug', $slug)->first();
        $reviews = DB::table('reviews')
            ->where('package_id', '=', $package->id)
            ->join('users', 'reviews.writer_id', '=', 'users.id')
            ->select('reviews.*', 'users.name as writer', 'users.image as image')
            ->orderBy('created_at', 'desc')
            ->get();
        $ratings = Rating::all();
        $related_package = Package::limit(3)->orderBy('name', 'asc')->get();
        return view('frontend.content.single_package', compact('package', 'reviews', 'ratings', 'related_package'));
    }

    //packages as per destination
    public function showPackages($slug)
    {
        $destination = Destination::where('slug', $slug)->first(); //destination for meta tag
        $packages = DB::table('packages')->where('destination_id', $destination->id)->orderBy('name', 'asc')->paginate(5);

        return view('frontend.content.package', compact('packages', 'destination'));
    }
}
