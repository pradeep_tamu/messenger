<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
        'trip_start_date',
        'number_of_people',
        'package_id',
        'adventure_id',
        'first_name',
        'last_name',
        'dob',
        'email',
        'phone',
        'country',
        'passport_number',
        'passport_expiration_date',
        'mailing_address',
        'emergency_contact_name',
        'emergency_contact_relation',
        'emergency_contact_phone',
        'arrival_date',
        'arrival_time',
        'arrival_flight_number',
        'pickup',
        'departure_date',
        'departure_time',
        'departure_flight_number',
        'drop_off',
        'insurance',
        'message',
        'reference',
        'status'
    ];
}
