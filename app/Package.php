<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use willvincent\Rateable\Rateable;

class Package extends Model
{
    use Rateable;

    protected $fillable = [
        'id',
        'name',
        'cost',
        'duration',
        'capacity',
        'destination_id',
        'description',
        'overview',
        'itineraries',
        'cost_details',
        'practical_info',
    ];

    protected $casts = [
        'image_name' > 'array',
    ];

    public function destination()
    {
        return $this->belongsTo('App\Destination', 'destination_id');
    }

    public function review(){
        return $this->hasMany('App\Review');
    }
}
